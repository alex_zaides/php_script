<?php
	//Сбор данных для передачи сервису
	//D.Obrazcov

	// Проверка на возможность запуска и запуск
	$output = array();
	$script_name = basename(__FILE__,'php').'ph[p]';
	exec("ps | grep -c '$script_name'",$output);
	if(!isset($output[0]) || $output[0] > 1)
	{
		echo "Already running...";
		return;
	}

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	require_once 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	set_time_limit(Config::$SCRIPT_TIMEOUT); //Устанавливаем таймаут
	register_shutdown_function('DataAccess::Abort');//Регистрируем корректное завершение
	
	CollectorManager::confirmEvent($_GET['eventList']);
?>