<?php
	//Сбор данных для передачи сервису
	//D.Obrazcov

	// Проверка на возможность запуска и запуск
	$output = array();
	$script_name = basename(__FILE__,'php').'ph[p]';
	exec("ps | grep -c '$script_name'",$output);
	if(!isset($output[0]) || $output[0] > 1)
	{
		echo "Already running...";
		return;
	}

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	require_once 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	set_time_limit(Config::$SCRIPT_TIMEOUT); //Устанавливаем таймаут
	register_shutdown_function('DataAccess::Abort');//Регистрируем корректное завершение
	
	//Проверка переинициализации контроллера
	$isReInit = CollectorManager::getControllerState();
	
	if(!$isReInit)
	{
		$result = "<D R='0' E='%s' P='%s' A='%d'/>";
		//События
		$event = CollectorManager::getEvent();
		//Данные
		$data = CollectorManager::getData();
		//Наличие готовых архивов
		$archCount = CollectorManager::getArchCount() > 0 ? 1:0;
		//Подготовка результата
		$result =sprintf($result,$event,$data,$archCount);
	}
	else 
		$result = "<D R='1'/>";
		
	//Сжатие результата и отправка
	print(gzdeflate($result,9));
?>