<?php

	// Получение статуса команд
	// Author D.Obrazcov

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	set_time_limit(600); //Устанавливаем таймаут
	register_shutdown_function('DataHelper::abort');//Регистрируем корректное завершение
	
	//Ответ CmdId|Result|EndExecDate|State
	$source = $_POST['source'];
	$idList = $_POST['commandList'];
	$result = array();
	$commandStr = "%d|%s|%s|%d";
	
	$dbResult = CommandManager::getCommandState($idList, $source);
	
	foreach ($dbResult as $row)
	{
		array_push($result, sprintf($commandStr,
												$row['CommandId']
												,$row['Result']
												,$row['EndExecDate']
												,$row['State']
												));
	}
	
	$result = sprintf("<OK R='%s'/>",implode("?$/",$result));
	
	print(gzdeflate($result,9));
?>