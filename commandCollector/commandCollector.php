<?php
	// Сборщик команд
	// Author D.Obrazcov
	
	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	set_time_limit(600); //Устанавливаем таймаут
	register_shutdown_function('DataHelper::abort');//Регистрируем корректное завершение
	
	// Формат команды:
	// CmdId|State|Type|Message|Priority|DeviceId
	
	$commands = $_POST['commandList']; // Список команд
	$source = $_POST['source']; // Источник команд
	$arrivalDate = Config::GET_DATE();
			
	$commandList = array();
	$commandsArr = explode("?$/", trim($commands,"?$/"));
	$res = '';
	$needrefresh = false;
	foreach ($commandsArr as $commandStr)
	{
		$row = explode('|',trim($commandStr,'|'));
		
		$command = new Command();
		$command->commandId = $row[0];
		$command->state = 5; // Состояние Sended
		$command->type = $row[2];
		$command->message = $row[3];
		$command->priority = $row[4];
		$command->deviceId = $row[5];
		$command->source = $source;
		$command->arrivalDate = $arrivalDate;
		if($command->type == 30){
			//$list = ChannelManager::getChannelById( $command->message);
			$list = ChannelManager::getChannelByDeviceId($command->deviceId, false, false);
			$res.= sprintf("<Command Id='%s' >", $command->commandId);
			foreach ($list as $channel)
			{
				$res.=  $channel->toXML(false);
				/*foreach($channel->EventLims as $eventLim)
				{
					$res.=$eventLim->toXML();
				}
				
				foreach($channel->EventVals as $eventVal)
				{
					$res.=$eventVal->toXML();
				}	*/			
				
			}
			$res.= sprintf("</Command>");
		}elseif($command->type == 31){
			$doc = simplexml_load_string($command->message);
			$userSetting=$doc[0];
			foreach ($userSetting->Channel as $channelXml){
				$channel = new ChanelInfo();
				$channel->ChanelId = (int)$channelXml['Id'];
				foreach ($channelXml->EventSetting as $eventSetting){
					$eventLim = new EventLimValue();
					$eventLim->Id = (string)$eventSetting['Id'];
					$eventLim->Type = (string)$eventSetting['Type'];
					$eventLim->Start = (string)$eventSetting['DRB'];
					$eventLim->Finish = (string)$eventSetting['DRE'];
					$eventLim->Value = (string)$eventSetting['Value'];
					$eventLim->IsHoliday = $eventSetting['H'];
					array_push($channel->EventVals,$eventLim);
				}
				ChannelManager::updateEventLim($channel->ChanelId,$channel->EventVals);
				$needrefresh = true;
			}
			$res.= sprintf("<Command Id='%s'></Command>", $command->commandId);
		}elseif($command->type == 32){//список праздничных дней
			$doc = simplexml_load_string($command->message);
			$userHoliday=$doc[0];
			$Year = $userHoliday->Year; 
			$Days = new Holidays($Year['V']);
			foreach ($Year->Day as $dayXML){
				$day = new Holiday();	
				$day->Days = $dayXML['V'];
				array_push($Days->Days,$day);
			}
			HolidayManager::SaveHoliday($Days);
			$needrefresh = true;
			$res.= sprintf("<Command Id='%s'></Command>", $command->commandId);
		}elseif($command->type == 33){//чтение списка праздничных дней		
			$list = HolidayManager::GetHolidays();
			$res .= sprintf("<Command Id='%s'>%s</Command>", $command->commandId,$list->toXML());
		}elseif($command->type == 40 || $command->type == 41){
			//включить/выключить опрос
			DeviceManager::changeActive($command->deviceId, 41 - $command->type);
			ControllerManger::reinitController();
			//Изменяем крон файл
			CronManager::createCronTab();
			DataHelper::executeNonQuery("vacuum");
			$needrefresh = true;
			
			$res.= sprintf("<Command Id='%s'></Command>", $command->commandId);
		}else		
		    array_push($commandList, $command);
	}
	
	// Сохранение команды
	CommandManager::saveCommands($commandList);
	if($needrefresh)   //Копируем на флэш
		exec(Config::SCRIPT_PATH().'toflash.sh -n');
	$res= sprintf("<OK>%s</OK>", $res);
	
	//LogManager::AddRecord(sprintf("Пытаюсь отправить уставки '%s'", $res), "Error");
	print(gzdeflate($res,9));
?>