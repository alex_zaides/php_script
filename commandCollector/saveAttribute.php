<?php
	
	//Сохранение атрибутов устройств
	//D.Obrazcov

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	set_time_limit(600); //Устанавливаем таймаут
	register_shutdown_function('DataHelper::abort');//Регистрируем корректное завершение
	
	//Формат атрибута
	//DeviceId||AttrId||Value
	
	$attrListStr = $_POST['attrList']; // Список атрибутов
	//$attrListStr = '9||209||3213?$/9||210||15?$/9||194||Ритекс-03';
	
	$attrList = array();
	$attrArr = explode("?$/", trim($attrListStr,"?$/"));
	
	foreach ($attrArr as $attrStr)
	{	
		if($attrStr == '')
			continue;
		$row = explode('||',trim($attrStr,'||'));
	
		$attribute = new Attribute();
		$attribute->deviceId = $row[0]; //Идентификатор устройства
		$attribute->attrId = $row[1]; //Идентификатор атрибута
		$attribute->value = $row[2]; //Значение атрибута
			
		array_push($attrList, $attribute);
	}
	
	if(count($attrList) > 0)
	{
		// Сохранение атрибутов
		DeviceManager::saveAttributes($attrList);
		
		//Оповещение адаптеров об изменении атрибутов
		$scriptName = Config::SCRIPT_PATH().'updateAttribute.sh';
		if(file_exists($scriptName))
			exec($scriptName);
	}
	
	print(gzdeflate("<OK/>",9));
?>