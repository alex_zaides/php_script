<?php

	// Удаление корректно завершенных команд
	// Author D.Obrazcov

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	set_time_limit(600); //Устанавливаем таймаут
	register_shutdown_function('DataHelper::abort');//Регистрируем корректное завершение
	
	$commandList = $_POST['commandList'];
	$source = $_POST["source"];
	
	CommandManager::removeCommand($commandList, $source);
	
	print(gzdeflate("<OK/>",9));
?>