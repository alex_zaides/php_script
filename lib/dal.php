<?php
	/**
	 * Класс для работы с БД
	 * @author dobrazcov
	 *
	 */
	class DataAccess 
	{
		/**
		 * Имя БД для подключения
		 * @var string
		 */
		private $db_name_;
		
		/**
		 * Объект доступа к БД
		 * @var PDO
		 */
		private $connection_;
		
		private function __construct($dbname) 
		{
			$this->db_name_ = $dbname;
			
			try 
			{
				$this->connection_ = new PDO("sqlite:$this->db_name_");
				$this->connection_->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->connection_->setAttribute(PDO::ATTR_TIMEOUT,Config::$LOCK_TIMEOUT);
			} 
			catch (Exception $e) 
			{
				LogManager::AddRecord($err->getMessage());
			}
		}

		function __destruct()
		{
			$this->connection_ = null;
		}
		
		/**
		 * Выполнение запроса без возврата результата
		 * @param string $query
		 * @return boolean
		 */
		public function execute($query) 
		{
			if(!isset($this->connection_))
				return false;
			
			$res = false;
			
			try 
			{
				if($this->connection_->beginTransaction())
				{
					$this->connection_->exec($query);
					$res = $this->connection_->commit();
				}
				else
					LogManager::AddRecord("Error open transaction DB:$this->db_name_");
			} 
			catch (Exception $e) 
			{
				$this->connection_->rollBack();
				LogManager::AddRecord("Error DB:$this->db_name_ $e->getMessage()");
			}
			
			return $res;
		}
		
		/**
		 * Выполнение запроса и возврат результата
		 * @param string $query
		 * @return Array
		 */
		public function select($query) 
		{
			if(!isset($this->connection_))
				return null;
				
			$res = null;
				
			try
			{
				if($this->connection_->beginTransaction())
				{
					$res = $this->connection_->query($query)->fetchAll();
					if(!$this->connection_->commit())
					{
						$res = null;
						LogManager::AddRecord("Error commit transaction DB:$this->db_name_");
					}
				}
				else
					LogManager::AddRecord("Error open transaction DB:$this->db_name_");
			}
			catch (Exception $e)
			{
				$this->connection_->rollBack();
				$res = null;
				LogManager::AddRecord("Error DB:$this->db_name_ $e->getMessage()");
			}
			
			return $res;
		}
		
		/**
		 * Имя БД
		 * @return string
		 */
		public function dbname()
		{					
			return $this->db_name_;
		}
		
		// Static realization --------------------------------
		
		/**
		 * Набор доступных соединений
		 * @var array
		 */
		private static $_cache = array();
		
		/**
		 * Получение соединения 
		 * @param string $dbname
		 * @return DataAccess
		 */
		public static function GetConnection($dbname)
		{
			if(!isset(self::$_cache) || empty($dbname) || !is_string($dbname))
				return NULL;

			$dbname = realpath($dbname);
			
			if(empty(self::$_cache[$dbname]))
				self::$_cache[$dbname] = new DataAccess($dbname);
			
			return self::$_cache[$dbname];
		}
		
		/**
		 * Закрытие соединения
		 * @param DataAccess $connection
		 */
		public static function CloseConnection(DataAccess &$connection)
		{
			if(!isset(self::$_cache) || !isset($connection))
				return;
			
			if(isset(self::$_cache[$connection->dbname()]))
				unset(self::$_cache[$connection->dbname()]);
				
			$connection = null;
		} 
				
		/**
		 * Обработчик внезапного прерывания скрипта
		 */
		public static function Abort()
		{
			if(isset(self::$_cache))
				self::$_cache = null;
		}
	}
?>