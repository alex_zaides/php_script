<?php
	
	/**
	 * Контроллер
	 * @author dobrazcov
	 */
	class Controler
	{
		/**
		 * Идентификатор контроллера
		 * @var int
		 */
		var $ControllerID = -1;
		
		/**
		 * Адрес контроллера
		 * @var string
		 */
		var $Address = 'Не указан';
		
		/**
		 * Временная зона
		 * @var int
		 */
		var $TimeZone = 0;
		
		/**
		 * Признак переинициализации
		 * @var int
		 */
		var $ReInit=0;
				
		//Сетевые настройки
		var $IP='';
		var $NetMask='';
		var $MAC='';
		var $NTPServer='';
		var $NetPriority='';
		var $CheckServerName='';
		var $HostService='';
		var $DNS1='';
		var $DNS2='';
		
		// eth
		var $Gateway='';
		var $UsePPPoE=false;
		var $PPPoEType='';
		var $PPPoELogin='';
		var $PPPoEPass='';
		var $PPPoEServer='';
		
		//PPP
		var $UsePPP=false;
		var $UsePPP2=false;
		var $PPPLogin='';
		var $PPPLogin2='';
		var $PPPPass='';
		var $PPPPass2='';
		var $PPPSerial='';
		var $PPPBaud='';
		var $APN='';
		var $APN2='';
				
		//VPN
		var $UseVPN=false;
		var $VPNType='';
		var $VPNLogin='';
		var $VPNLogin2='';
		var $VPNPWD='';
		var $VPNPWD2='';
		var $VPNConnectServer='';
		var $VPNConnectServer2='';
		var $VPNPort=0;
		var $VPNPort2=0;
		var $VPNKeyFileName='';
		
		/**
		 * Устройства
		 * @var array
		 */
		var $Devices = array();
		
		/**
		 * Описание контроллера и устройств
		 */
		function toXML()
		{
			$XML="<Controller Ver='2' ControllerId='%d' Address='%s' TimeZone='%d'>";
			$XML=sprintf($XML,$this->ControllerID,$this->Address,$this->TimeZone);
			
			// данные устройств
			foreach ($this->Devices as $dev)
			{
				$XML.=$dev->toXML();
			}
			$XML.="</Controller>";
			return $XML;
		}
		
		/**
		 * Сетевые настройки контроллера
		 */
		function toNetSettingXML()
		{
			$XML="<Controller Ver='2' IP='%s' MAC='%s' DNS1='%s' DNS2='%s' NetMask='%s' ".
			"Gateway='%s' NTPServer='%s' NETPriority='%s' ".
			"UsePPP='%d' UsePPP2='%d' PPPLogin='%s' PPPLogin2='%s' PPPPass='%s' PPPPass2='%s' PPPSerial='%s' PPPBaud='%s' APN='%s' APN2='%s' CheckServerName='%s' ".
			"UseVPN='%d' VPNType='%s' VPNLogin='%s' VPNPWD='%s' VPNKeyFileName='%s' VPNConnectServer='%s' VPNConnectServer2='%s' VPNPort='%d' VPNPort2='%d' HostService='%s' ".
			" UsePPPoE='%d' PPPoEType='%s' PPPoELogin='%s' PPPoEPass='%s' PPPoEServer='%s' ".
			"/>";
			$XML=sprintf($XML,
					$this->IP
					,$this->MAC
					,$this->DNS1
					,$this->DNS2
					,$this->NetMask
					,$this->Gateway
					,$this->NTPServer
					,$this->NetPriority
					,$this->UsePPP
					,$this->UsePPP2
					,$this->PPPLogin
					,$this->PPPLogin2
					,$this->PPPPass
					,$this->PPPPass2
					,$this->PPPSerial
					,$this->PPPBaud
					,$this->APN
					,$this->APN2
					,$this->CheckServerName
					,$this->UseVPN
					,$this->VPNType
					,$this->VPNLogin
					,$this->VPNPWD
					,$this->VPNKeyFileName
					,$this->VPNConnectServer
					,$this->VPNConnectServer2
					,$this->VPNPort
					,$this->VPNPort2
					,$this->HostService
					,$this->UsePPPoE
					,$this->PPPoEType
					,$this->PPPoELogin
					,$this->PPPoEPass
					,$this->PPPoEServer
		);
			return $XML;
		}
		
		/**
		 * Сетевые настройки для адаптера
		 */
		function NETSettings()
		{
			$result = "prior ".$this->NetPriority;
			$result .= "\nip ".$this->IP;
			$result .= "\ngw ".$this->Gateway;
			$result .= "\ndns1 ".$this->DNS1;
			$result .= "\ndns2 ".$this->DNS2;
			$result .= "\nmask ".$this->NetMask;
			$result .= "\nmac ".$this->MAC;
			$result .= "\nntp ".$this->NTPServer;
			$result .= "\nppp_gprs1 ".$this->UsePPP;
			$result .= "\nppp_gprs2 ".$this->UsePPP2;
			$result .= "\nppp_apn1 ".$this->APN;
			$result .= "\nppp_apn2 ".$this->APN2;
			$result .= "\nppp_lgn1 ".$this->PPPLogin;
			$result .= "\nppp_lgn2 ".$this->PPPLogin2;
			$result .= "\nppp_psw1 ".$this->PPPPass;
			$result .= "\nppp_psw2 ".$this->PPPPass2;
			$result .= "\nppp_port ".$this->PPPSerial;
			$result .= "\nppp_baud ".$this->PPPBaud;
			$result .= "\nppp_check ".$this->CheckServerName;
			$result .= "\nvpn_use ".$this->UseVPN;
			$result .= "\nvpn_type ".$this->VPNType;
			$result .= "\nvpn_lgn1 ".$this->VPNLogin;
			$result .= "\nvpn_lgn2 ".$this->VPNLogin2;
			$result .= "\nvpn_psw1 ".$this->VPNPWD;
			$result .= "\nvpn_psw2 ".$this->VPNPWD2;
			$result .= "\nvpn_srv1 ".$this->VPNConnectServer;
			$result .= "\nvpn_srv2 ".$this->VPNConnectServer2;
			$result .= "\nvpn_port1 ".$this->VPNPort;
			$result .= "\nvpn_port2 ".$this->VPNPort2;
			$result .= "\ndb_srv ".$this->HostService;
			$result .= "\ntime_zone ".$this->TimeZone;
			$result .= "\npppoe_use ".$this->UsePPPoE;
			$result .= "\npppoe_type ".$this->PPPoEType;
			$result .= "\npppoe_srv ".$this->PPPoEServer;
			$result .= "\npppoe_lgn ".$this->PPPoELogin;
			$result .= "\npppoe_psw ".$this->PPPoEPass;
						
			return $result;
		}
	}
	 
	/**
	 * 
	 * Адаптер
	 * @author dobrazcov
	 *
	 */
	class Adapter
	{
		var $FileName = '';
		var $AdapterInfo = '';
		var $ParamInfo = '';
		var $AdditionalArgument = '';
		
		function toXML()
		{
			$XML="<Adapter FileName='%s' AdapterInfo='%s' ParamInfo='%s' AdditionalArgument='%s' />";
			$XML=sprintf($XML,$this->FileName,$this->AdapterInfo,$this->ParamInfo, $this->AdditionalArgument);
			return $XML;
		}
	} 
	
	/**
	 * Устройство
	 * @author dobrazcov
	 *
	 */
	class Device
	{
		/**
		 * Идентификатор устройства
		 * @var int
		 */
		var $DeviceId = -1;
		
		/**
		 * Адрес установки устройства
		 * @var string
		 */
		var $Address='Не указан';
		
		/**
		 * 
		 * Частота опроса устройства (мин.)
		 * @var int
		 */
		var $Frequency = 30;
		
		/**
		 * Имя файла адаптера
		 * @var string
		 */
		var $AdapterName = 'Не указан';
		
		/**
		 * Сенсоры устройства
		 * @var array
		 */
		var $Sensors = array();
		
		/**
		 * 
		 * Аргументы запуска
		 * @var array
		 */
		var $Arguments = array();
		/**
		 *
         * Включить опрос устройства
		*/		
		var $IsOn;
		/**
		 * Получение описания устройства
		 */
		function toXML()
		{
			$XML="<Device DeviceId='%d' Address='%s' Frequency='%d' AdapterName='%s' IsOn='%d'>";
			$XML=sprintf($XML,$this->DeviceId,$this->Address,$this->Frequency,$this->AdapterName, $this->IsOn);
			
			//Сенсоры
			foreach ($this->Sensors as $sensor)
			{
				$XML .= $sensor->toXML();
			}

			//Аргументы
			foreach ($this->Arguments as $arg)
			{
				$XML .= $arg->toXML();
			}
						
			$XML.="</Device>";
			return $XML;
		}
	} 

	/**
	 * 
	 * Аргументы запуска
	 * @author dobrazcov
	 *
	 */
	class AdditionalArgument {
		
		/**
		 * 
		 * Идентифкатор устройства
		 * @var int
		 */
		var $DeviceId = -1;
		
		/**
		 * 
		 * Название аргумента
		 * @var string
		 */
		var $Name = "";
		
		/**
		 * 
		 * Значение аргумента
		 * @var string
		 */
		var $Value = "";
		
		/**
		 * 
		 * XML описание
		 */
		public function toXML() 
		{
			$XML = "<AdditionalArgument DeviceId='%d' Name='%s' Value='%s'/>";
			return sprintf($XML,$this->DeviceId,$this->Name,$this->Value);
		}
	}
	
	/**
	 * Сенсор
	 * @author dobrazcov
	 */
	class SensorInfo
	{
		/**
		 * Идентификатор устройста
		 * @var int
		 */
		var $DeviceId=-1;
		
		/**
		 * Идентификатор сенсора
		 * @var int
		 */
		var $SensorId=-1;
		
		/**
		 * Адрес установки
		 * @var string
		 */
		var $Address='Не указан';
		
		/**
		 * Ключ сенсора
		 * @var string
		 */
		var $SerialKey='';
		
		/**
		 * Получение описания сенсора
		 */
		function toXML()
		{
			$XML="<Sensor SensorId='%d'  DeviceId='%d' Address='%s' SerialKey='%s' >";
			$XML=sprintf($XML,$this->SensorId,$this->DeviceId,$this->Address, $this->SerialKey);
			$XML.="</Sensor>";
			return $XML;
		}
	}

	/**
	 * 
	 * Канал
	 * @author dobrazcov
	 *
	 */
	class ChanelInfo
	{
		var $ChanelId=0;
		var $ParamId=0;
		var $OverridenParamId=-1;
		var $SensorId=0;
		var $DeviceId=0;
		var $SerialKey = '';
		var $Address='';
		var $InterfaceArgument1;
		var $InterfaceArgument2;
		var $InterfaceArgument3;
		var $InterfaceArgument4;
		var $IsEvent = 0;
		var $IsOn = 1;
		var $EventLims = array();
		var $EventVals = array();
		
		function toXML($isfull = true)
		{
			$XML="";
			if($isfull){
			$XML="<Channel ChanelId='%d' ParamId='%d' OverridenParamId='%d' SensorId='%d' DeviceId='%d' Address='%s'
						 InterfaceArgument1='%s' InterfaceArgument2='%s' InterfaceArgument3='%s' InterfaceArgument4='%s' IsOn='%d' IsEvent='%d' 
						 SerialKey='%s' >";
			
			$XML=sprintf($XML,
					$this->ChanelId
					,$this->ParamId
					,$this->OverridenParamId
					,$this->SensorId
					,$this->DeviceId
					,$this->Address
					,$this->InterfaceArgument1
					,$this->InterfaceArgument2
					,$this->InterfaceArgument3
					,$this->InterfaceArgument4
					,$this->IsOn
					,$this->IsEvent
					,$this->SerialKey);
			}else{
				if(count($this->EventLims) == 0 && count($this->EventVals) == 0) return "";	
				$XML="<Channel ChanelId='%d'  >";				
				$XML=sprintf($XML,	$this->ChanelId	);	
			}
					
			foreach($this->EventLims as $eventLim)
			{
				$XML.=$eventLim->toXML();
			}

			foreach($this->EventVals as $eventVal)
			{
				$XML.=$eventVal->toXML();
			}

			$XML .= "</Channel>";
			
			return $XML;
		}
		
		//Получение настроек в формате для контроллера
		function getSettingsForController() 
		{
			$settings = "%s|%d|%d|%s|%s|%s|%s|%d";
			$settings = sprintf($settings
								,$this->SerialKey
								,$this->ParamId
								,$this->ChanelId
								,$this->InterfaceArgument1
								,$this->InterfaceArgument2
								,$this->InterfaceArgument3
								,$this->InterfaceArgument4
								,$this->IsEvent);
			foreach ($this->EventLims as $eventLim)
			{
				$settings .= "|".$eventLim->toControllerString();
			}
			return $settings;
		}
	}
	
	/**
	 * 
	 * Пользователь системы
	 * @author dobrazcov
	 *
	 */
	class UserInfo
	{
		var $UserID=0;
		var $Tag='';
		var $Name='new  user';
		var $Login='new login';
		var $PWD='';
		var $Version='';
		
		function toString()
		{
			return $this->UserID." ".$this->Name;
		}
		
		function toXML()
		{
			$XML="<User UserId='%d' Name='%s' Role='%s' Ver='%s' />";
			$XML=sprintf($XML,$this->UserID,$this->Name,$this->Tag,$this->Version);
			return $XML;
		}
		
	}
	
	/**
	 * 
	 * Ошибка
	 * @author dobrazcov
	 *
	 */
	class Error
	{
		var $description=""; 
	
		function Error($description)
		{
			$this->description=$description;
			
		}
		function toXML()
		{
			return "<Error message='".$this->description."' />";
		}
	}
	
	// события
	class Event 
	{
		var $EventId = 0;
		var $TypeId = 0;
		var $DataSrcId = 0;
		var $RegisterDate = '';
		var $Argument1 ='';
		var $Argument2 = '';
		var $Argument3 = '';
		var $Argument4 = '';
		
		function toXML() 
		{
			$XML="<Event EventId='%d' TypeId='%d' DataSrcId='%d' RegisterDate='%s' Argument1='%s' Argument2 = '%s' Argumnet3 = '%s' Argument4 = '%s' />";
			$XML=sprintf($XML,
					$this->EventId,
					$this->TypeId,
					$this->DataSrcId,
					$this->RegisterDate,
					$this->Argument1,
					$this->Argument2,
					$this->Argumnet3,
					$this->Argument4
					);
			return $XML;
		}
	}
	
	//Значения параметров
	class ParamData 
	{
		var $ParamId = 0;
		var $DataSrcId = 0;
		var $RegisterDate = '';
		var $Value = 0.0;		
	}
	/**	
	 * Праздник
	*/
	class Holiday
	{
		var $Days;
		public function toXML() 
		{
			$XML = "<Day V='%s'/>";
			return sprintf($XML,$this->Days);
		}
	}	
	/**
	 * Список праздничных дней
	*
	*/
	class Holidays
	{
		var $Year;	
		var $Days= array();
		public function __construct($year)
		{
			$this->Year = $year;
		}

		public function toXML() 
		{
			$XML = "<Data>
						<Year V='%s'>";
			foreach($this->Days as $Day)
			{
				$XML .= $Day->toXML();
			}
			$XML .= "	</Year>
					</Data>";
			return sprintf($XML,$this->Year);
		}
	}
	/**
	 * Граничное значение события
	 * @author dobrazcov
	 *
	 */
	class EventLimValue 
	{
		/**
		 * Идентификатор
		 * @var integer
		 */
		var $Id = '';	

		/**
		 * Тип уставки
         * @var integer
		*/	
		var $Type;
		
		/**
		 * Время начала действия
		 * @var string
		 */
		var $Start;
		
		/**
		 * Время окончания действия
		 * @var string	
		*/
		var $Finish;
		/**
		 * Граничное значение
		 * @var string
		 */
		var $Value;	
		
		/**
		 * Признак праздничного дня
	     */
		var $IsHoliday;
		
		/**
		 * Получение граничных данных в формате для контроллера
		 */
		public function toControllerString() 
		{
			return $this->Id."$".$this->Value."$".$this->Type."$".$this->Start."$".$this->Finish."$".$this->IsHoliday;
		}
		
		/**
		 * Получение XML
		 */
		public function toXML() 
		{
			$XML = "<EventSetting Id='%s' Value='%s' Type='%s' DRB='%s' DRE='%s' H='%s' />";
			return sprintf($XML,$this->Id,$this->Value, $this->Type, $this->Start, $this->Finish, $this->IsHoliday);
		}
		}

	/**
	 * Класс типа уставки
	 * @author zaides
	 */
	class EventLimType
	{	
		/**
		 * Идентификатор
		 * @var integer
		*/
		var $Id;	
		
		/**
		 * Тип уставки
         * @var integer
		*/	
		var $Type;
		
		/**
		 * Параметры уставки
		 * @var string
		*/
		var $Tag;
		
		/**
		 * Получение граничных данных в формате для контроллера
		 */
		public function toControllerString() 
		{
			return $this->Id."$".$this->Type."$".$this->Tag;
	}
	
	/**
		 * Получение XML
		 */
		public function toXML() 
		{
			$XML = "<EventSettingType Param='%s' Id='%s' Tag='%s' />";
			return sprintf($XML,$this->Id,$this->Type, $this->Tag);
		}
	}
	/**
	 * Класс команды
	 * @author dobrazcov
	 */
	class Command 
	{
		/**
		 * Идентификатор команды
		 * @var int
		 */
		var $commandId = 0;
		
		/**
		 * Тип команды
		 * @var int
		 */
		var $type = 0;
		
		/**
		 * Источник команды
		 * @var string
		 */
		var $source = '';
		
		/**
		 * Состояние команды
		 * @var int
		 */
		var $state = 0;
		
		/**
		 * Приоритет команды
		 * @var int
		 */
		var $priority = 0;
		
		/**
		 * Идентификатор команды
		 * @var int
		 */
		var $deviceId = 0;
		
		/**
		 * Сообщение команды
		 * @var string
		 */
		var $message = '';
		
		/**
		 * Дата прибытия команды на ИК
		 * @var DateTime
		 */
		var $arrivalDate = '';
		
		/**
		 * Дата завершения команды
		 * @var DateTime
		 */
		var $endExecDate = '';
		
		/**
		 * Результат выполнения команды
		 * @var string
		 */
		var $result = '';
	}
	
	/**
	 * Атрибут устройства
	 * @author dobrazcov
	 */
	class Attribute
	{
		/**
		 * Идентификатор устройства
		 * @var int
		 */
		var $deviceId = -1;
		
		/**
		 * Идентификатор атрибута
		 * @var int
		 */
		var $attrId = 0;
		
		/**
		 * Значение атрибута
		 * @var string
		 */
		var $value = '';
		
	}
?>