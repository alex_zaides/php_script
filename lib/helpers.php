<?php

	/**
	 * Хелпер для работы с БД
	 * @author dobrazcov
	 *
	 */
	class DataHelper
	{
		/**
		 * Объект подключения к БД
		 * @var PDO
		 */
		private static $db = null;
		
		/**
		 * Подключение к БД
		 * @param string Строка подключения $connectionString
		 * @return PDO Созданное подключение
		 */
		private static function getConnection($connectionString)
		{
			try 
			{
				self::$db = new PDO('sqlite:'.$connectionString);
				self::$db->setAttribute(PDO::ATTR_TIMEOUT,Config::$LOCK_TIMEOUT);
				self::$db->beginTransaction();
			    return self::$db;
			}
			catch (Exception $e)
			{
				self::logError($e);
			}
		}
		
		/**
		 * Логирование ошибки
		 * @param Exception $e Ошибка
		 * @param boolean $withRollback С откатом транзакции
		 */
		private static function logError(Exception $e, $withRollback = true)
		{
			if(isset($e))
				LogManager::AddRecord($e->getMessage());
			
			if($withRollback && isset(self::$db))
			{
				try 
				{
					self::$db->rollBack();
				} 
				catch (Exception $err) 
				{
					LogManager::AddRecord($err->getMessage());
				}
			}
		}

		/**
		 * Выполнить без получения результата (база с настройками)
		 * @param string Запрос $SQL
		 */
		public static function executeNonQuery($SQL)
		{
			try 
			{
				DataHelper::getConnection(Config::DB_NAME());
				self::$db->exec($SQL);
				self::$db->commit();
			} 
			catch (Exception $e) 
			{
				self::logError($e);
			}
			self::$db = null;
		}
				
		/**
		 * Выполнить с получением результата (база с настройками)
		 * @param string Запрос $SQL
		 * @return array
		 */
		public static function executeQuery($SQL)
		{
			try
			{
				DataHelper::getConnection(Config::DB_NAME());
				$query = self::$db->query($SQL);
				$result = is_object($query) ? $query->fetchAll():null;
				self::$db->commit();
			}
			catch (Exception $e)
			{
				self::logError($e);
			}
			
			self::$db = null;
			
			return $result;
		}
				
		/**
		 * Выполнить с получением результата (база с данными)
		 * @param string Запрос $SQL
		 * @param int $resultMode
		 * @return array
		 */
		public static function executeQueryOnDataDB($SQL, $resultMode = PDO::FETCH_BOTH)
		{
			try
			{
				DataHelper::getConnection(Config::DB_DATA_NAME());
				$result = self::$db->query($SQL)->fetchAll($resultMode);
				self::$db->commit();
			}
			catch (Exception $e)
			{
				self::logError($e);
			}
			self::$db = null;
			
			return $result;
		}
		
		/**
		 * Выполнить без получения результата (база с данными)
		 * @param string Запрос $SQL
		 */
		public static function executeNonQueryOnDataDB($SQL)
		{
			try
			{
				DataHelper::getConnection(Config::DB_DATA_NAME());
				self::$db->exec($SQL);
				self::$db->commit();
			}
			catch(Exception $e)
			{
				self::logError($e);
			}
			
			self::$db = null;
		}
		
		/**
		 * Выполнить c получением результата (база с событиями)
		 * @param string Запрос $SQL
		 * @return array
		 */
		public static function executeQueryOnEventDB($SQL)
		{
			try
			{
				DataHelper::getConnection(Config::DB_EVENT_NAME());
				$result = self::$db->query($SQL)->fetchAll();
				self::$db->commit();
			}
			catch(Exception $e)
			{
				self::logError($e);
			}
			self::$db = null;
			
			return $result;
		}
		
		/**
		 * Выполнить без получения результата (база с событиями)
		 * @param string Запрос $SQL
		 */
		public static function executeNonQueryOnEventDB($SQL)
		{
			try
			{
				DataHelper::getConnection(Config::DB_EVENT_NAME());
				self::$db->exec($SQL);
				self::$db->commit();
			}
			catch(Exception $e)
			{
				self::logError($e);	
			}
			self::$db = null;
		}
		
		/**
		 * Начало эксклюзивной транзакции на базе с данными
		 */
		public static function beginTransaction() 
		{
			try
			{
				self::$db = new PDO('sqlite:'.Config::DB_DATA_NAME());
				self::$db->setAttribute(PDO::ATTR_TIMEOUT,Config::$LOCK_TIMEOUT);
				self::$db->exec('BEGIN IMMEDIATE');
			}
			catch(Exception $e)
			{
				self::logError($e);
				self::$db = null;
			}
		}
		
		/**
		 * Коммит транзакции на базе с данными
		 */
		public static function commitTransaction() 
		{
			try
			{
				if(isset(self::$db))
					self::$db->exec('COMMIT');
			}
			catch(Exception $e)
			{
				self::logError($e);
			}
			
			self::$db = null;
		}
		
		/**
		 * Откат транзакции на базе с данными
		 */
		public static function rollBackTransaction() 
		{
			try
			{
				if(isset(self::$db))
					self::$db->exec('ROLLBACK');
			}
			catch(Exception $e)
			{
				self::logError($e);
			}
			
			self::$db = null;
		}
		
		/**
		 * Выполнение скрипта в транзакции без получения результата (база с данными)
		 * @param string Запрос $SQL
		 */
		public static function execInTransactionNonQuery($SQL) 
		{
			try
			{
				if(isset(self::$db))
					self::$db->exec($SQL);
			}
			catch(Exception $e)
			{
				self::logError($e);
			}
		}
		
		/**
		 * Выполнение скрипта в транзакции c получением результата (база с данными)
		 * @param string Запрос $SQL
		 * @return array
		 */
		public static function execInTransactionQuery($SQL) 
		{
			try
			{
				if(isset(self::$db))
					$result = self::$db->query($SQL)->fetchAll();
			}
			catch(Exception $e)
			{
				self::logError($e);
				self::$db = null;
			}
			
			return $result;
		}
		
		/**
		 * Прерывание подключения
		 */
		public static function abort()
		{
			$stat = connection_status();
			if ($status == 2 || $status == 3 || connection_aborted())
			{
				if(isset(self::$db))
				{
					try 
					{
						self::$db->rollback();
					} 
					catch (Exception $e) 
					{
						self::logError($e,false);
					}
				}
				self::$db = null;
			}
		}
	}
?>