<?php
	/**
	 * 
	 * Конфигурационный файл
	 * @author D.Obrazcov
	 *
	 */
	class Config
	{
		//******* Все пути к файлам указываются относительно директории файла config.php *********//
		//****************************************************************************************//
		
		//путь к файлу с описанием менеджеров
		public static function MANAGER_PATH(){return self::THIS_PATH()."managers.php";}
		
		//путь к файлу с описанием бизнес объектов
		public static function DOMAIN_PATH(){return self::THIS_PATH()."buisnessClasses.php";}
		
		//путь к файлу с описанием хелпера
		public static function HELPER_PATH(){ return self::THIS_PATH()."helpers.php";}
		
		//путь к файлу с доступом к БД
		public static function DAL_PATH(){ return self::THIS_PATH()."dal.php";}

		// путь к файлу БД (в оперативке)
		public static function DB_NAME(){return self::THIS_PATH()."../data/ic_data3.sdb";}
		
		// путь к файлу БД (во flash)
		public static function DB_NAME_BACKUP(){return self::THIS_PATH()."../data2/ic_data3.sdb";}
	
		// путь к файлу БД со значениями параметров (в оперативке)
		public static function DB_DATA_NAME(){return self::THIS_PATH()."../data/ic_data_value3.sdb";}
		
		// Путь к файлу БД с событиями (в оперативке)
		public static function DB_EVENT_NAME_REST(){ return self::THIS_PATH()."../data/ic_data_event3.sdb";}
		
		//********************** Настройки устанавливаемые при разворачивании на контроллере **********************************//
		//*********************************************************************************************************************//
		
		// Путь к файлу с бэкапом на флеш (!!! Задается при установке)
		public static function DB_BACKUP_NAME(){return "/forsrv/backup/ic_data_value3.sdb";}
		
		// Путь к файлу БД с событиями (!!! Задается при установке)
		public static function DB_EVENT_NAME(){ return "/forsrv/event/ic_data_event3.sdb";}
		
		// Путь к файлу для запуска crontab (!!! Задается при установке)
		public static function CRON_FILE(){ return "/var/log/cron/crontabs/root";}
		
		// Путь к файлу крона на флэш (!!! Задается при установке)
		public static function CRON_FILE_FLASH(){ return "/mnt/crontabs/root";}
		
		// максимальный объем записей БД (Байт) (одна запись 45 байт)
		public static $DB_DATA_MAX = 500;
	
		// рамер архивов с данными БД (Байт) (сжатие примерно в 230 раз)
		public static $PACKAGE_SIZE = 100; 
	
		// Путь к архивам БД (!!! Задается при установке)
		public static function DB_ARCH_PATH(){return "/forsrv/archive/";}
		
		// путь к директории с ключем VPN(!!! Задается при установке)
		public static function VPN_KEY_PATH(){return "/etc/vpn/";}
	
		// путь директории с адаптерами (!!! задается при установке)
		public static function ADAPTERS_PATH(){return "/mnt/adapters/";}
		
		// путь к лог файлу (!!! Задается при установке)
		public static function LOG_FILE_PATH(){return "/logs/";}
		
		// путь к списку адаптеров автозапуска
		public static function AUTORUN_FILE(){return  "/mnt/scripts/add_start.sh";}
		
		// путь к директории со скриптами
		public static function SCRIPT_PATH(){return  "/mnt/scripts/";}
		
		// путь к гейтвею
		public static function DBGATEWAY_PATH(){return  "/mnt/dbgateway/dbgateway2";}
		
		// ожидание получения блокировки для записи данных (в секундах)
		public static $LOCK_TIMEOUT = 2;
		/**
		 * Максимальное время выполнение скрипта (сек)
		 * @var int
		 */
		public static $SCRIPT_TIMEOUT = 600;
		
		/**
		 * 
		 * Подключение файлов проекта
		 */
		public static function IncludeFiles()
		{
			require_once self::MANAGER_PATH();
			require_once self::DOMAIN_PATH();
			require_once self::HELPER_PATH();
			require_once self::DAL_PATH();
		}
		
		private static $THIS_PATH;//Директория с текущим файлом
		
		/**
		 * 
		 * Директория с текущим файлом
		 */
		private static function THIS_PATH()
		{
			if(!isset(self::$THIS_PATH))
				self::$THIS_PATH = dirname(__FILE__)."/";
			return self::$THIS_PATH;
		}
		
		/**
		 * Получение текущей даты
		 */
		public static function GET_DATE() {
			$output = array();
			exec('date +"%Y-%m-%d %H:%M:%S"',$output);
			return $output[0];		
		}
	}
?>