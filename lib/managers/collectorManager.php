<?php
	/**
	 * 
	 * Менеджер подготовки данных для сервиса сбора
	 * @author D.Obrazcov
	 *
	 */
	class CollectorManager {
		
		/**
		 * 
		 * Возвращает состояние контроллера (переинициализирован или нет)
		 */
		static public function getControllerState() 
		{
			$DB = DataAccess::GetConnection(Config::DB_NAME());
			$result = $DB->select("select ReInitialized from tblControllerInfo");
			DataAccess::CloseConnection($DB);
						
		    $arr = $result[0];
		    return (boolean) $arr['ReInitialized'];
		}
		
		/**
		 * 
		 * Возвращает метаданные
		 */
		static public function getMetaData() 
		{
			//Итоговый результат
			$XML = "<M A='%s' T='%d' D='%s' S='%s' C='%s' CMD='%s'/>";
			
			$controllerAddress = '';
			$controllerTimeZone = 0;
			$devices = '';
			$sensors = '';
			$channels = '';
			
			$DB = DataAccess::GetConnection(Config::DB_NAME());
			
			//Получение информации по контроллеру
			$SQL = "select Address, TimeZone from tblControllerInfo";
			
			$result = $DB->select($SQL);
			
			if(($arr = $result[0])!=null)
			{
				$controllerAddress = $arr['Address'];
				$controllerTimeZone = $arr['TimeZone'];
			}
			
			//Получение информации по устройствам
			$SQL = "select DeviceId, Address, AdapterName, IsOn from tblDeviceInfo";
			
			$result = $DB->select($SQL);
			$cmdInfo = Config::ADAPTERS_PATH()."%s -cmd";
			$adapterInfo = Config::ADAPTERS_PATH()."%s -p";
			$output = array();
			$cmdList = '';
			foreach ($result as $arr)
			{
				$adapterName = '';
				$adapterVersion = '';
				
				$adapterName = $arr["AdapterName"];
				
				unset($output);
				exec(sprintf($adapterInfo,$adapterName),$output);
				
				if(count($output) > 0)
				{
					$str = explode('|', $output[0]);
					if(count($str) === 3)
					{
						$adapterVersion = $str[0].' - Ver. '.$str[1];
					}
				}
				
				$devices .= $arr["DeviceId"].'|'.$arr['Address'].'|'.$adapterVersion.'|'.$arr['IsOn'].'~';
				
				// Получение информации о команде адаптера
				unset($output);
				exec(sprintf($cmdInfo,$adapterName),$output);
				foreach ($output as $item)
				{
					if(strpos($item, '~') === 0)
					{
						// Аргумент команды
						$cmdList .= '||'. trim($item,'~');
					}
					else 
					{
						// Команда
						$cmdDesc = explode('|',$item);
						$cmdList .= '~~'.$arr["DeviceId"].'||'.$cmdDesc[0];
					}
				}
				$cmdList .= '~~'.$arr["DeviceId"].'||30~~'.$arr["DeviceId"].'||31~~'.$arr["DeviceId"].'||40~~'.$arr["DeviceId"].'||41~~'.$arr["DeviceId"].'||32~~';
			}
			
			$devices = trim($devices,'~');
			$cmdList = trim($cmdList,'~~');
			
			//Получение информации по сенсорам
			$SQL = "select DeviceId, SensorId, SensorAddress from tblSensorInfo";
			
			$result = $DB->select($SQL);
			
			foreach ($result as $arr)
			{
				$sensors .= $arr["DeviceId"].'|'.$arr['SensorId'].'|'.$arr['SensorAddress'].'~';
			}
			
			$sensors = trim($sensors,'~');
			
			//Получение информации по каналам
			$SQL = "select 
						DeviceId
						,case
							when SensorId = -1 then 0
							else SensorId
						end as SensorId
						,ChanelId
						,case
							when OverridenParamId = -1 then ParamId
							else OverridenParamId
						end as ParamId
						,Address
						,IsOn
						,IsEvent
					from tblChanelInfo";
			
			$result = $DB->select($SQL);
			
			foreach ($result as $arr)
			{
				$channels .= $arr['DeviceId'].'|'.$arr['SensorId'].'|'.$arr['ChanelId'].'|'.$arr["ParamId"].'|'.$arr['Address'].'|'.$arr['IsOn'].'|'.$arr['IsEvent'].'~';
			}
			
			$channels = trim($channels,'~');
			
			//Сброс признака переинициализации
			$SQL = "update tblControllerInfo set ReInitialized = 0";
			
			$DB->execute($SQL);
			DataAccess::CloseConnection($DB);
			
			//На флешке
			$DB = DataAccess::GetConnection(Config::DB_NAME_BACKUP());
			$DB->execute($SQL);
			DataAccess::CloseConnection($DB);
			
			return sprintf($XML,$controllerAddress,$controllerTimeZone,$devices,$sensors,$channels,$cmdList);
		}
				
		/**
		 * 
		 * Возвращает архив с данными
		 */
		static public function getData()
		{
			//Проверяем на наличие архивов
			$archCount = self::getArchCount();
			if($archCount > 0)
			{
				//Пытаемся получить архив
				$result = self::getArch();
			}
			else
			{
				//Формируем архив на лету из гейтвея
				exec(Config::DBGATEWAY_PATH()." -g",$result);
			}
	
			$resultStr = ''; //Результат который будет сжиматься
		
			foreach ($result as $str)
			{	
				if( strpos($str,'RDF') !== false || strpos($str,'};') !== false )
					continue;
				$str = trim(trim($str),"{},");
				$str = str_replace('"', '', str_replace(',','|',preg_replace('/\s(?=([^"]*"[^"]*")*[^"]*$)/','',$str)));
				$resultStr .= '~'.$str;
			}
			
			$resultStr = trim($resultStr,'~');
			
			return $resultStr;									
		}
		
		/**
		 * 
		 * Получение кол-ва готовых к отправке архивов
		 */
		static public function getArchCount()
		{
			return count(glob(Config::DB_ARCH_PATH()."*.gz"));
		}
		
		/**
		 * 
		 * Возвращает накопленные события
		 */
		static public function getEvent()
		{
			$script = " select
							EventId 
		   					,ChannelId 
		   					,TypeId 
		   					,RegisterDate 
		   					,case when Argument1 is null then '' else Argument1 end as Argument1
							,case when Argument2 is null then '' else Argument2 end as Argument2
							,case when Argument3 is null then '' else Argument3 end as Argument3
							,case when Argument4 is null then '' else Argument4 end as Argument4 
							,StartEventId
		     			from tblEventBus 
		     			where IsSending = 0
		     			order by EventId";
			
			//Выполнение запроса
			$DB = DataAccess::GetConnection(Config::DB_EVENT_NAME());
			$dbResult = $DB->select($script);
			DataAccess::CloseConnection($DB);
	
			//Обработка результата
			$result = '';
			$argument1 = '';
			$argument2 = '';
			$argument3 = '';
			$argument4 = '';
			foreach ($dbResult as $row)
			{
				$argument1 = $row['Argument1'];
				if($argument1 === '')
					$argument1 = 'X';
				$argument2 = $row['Argument2'];
				if($argument2 === '')
					$argument2 = 'X';
				$argument3 = $row['Argument3'];
				if($argument3 === '')
					$argument3 = 'X';
				$argument4 = $row['Argument4'];
				if($argument4 === '')
					$argument4 = 'X';
			
				$result .= "~".$row['EventId'].'|'.$row['ChannelId'].'|'.$row['TypeId'].'|'.$row['RegisterDate'].'|'.$argument1.'|'.$argument2.'|'.$argument3.'|'.$argument4.
							'|'.$row['StartEventId'];
			}
			
			$result = trim($result,'~');
			
			return $result;
		}
		
		/**
		 * 
		 * Подтверждение принятия событий сервисом
		 * @param string $eventList Список принятых событий
		 */
		static public function confirmEvent($eventList) {
			
			$eventList = trim($eventList,',');
			
			if(empty($eventList))
				return;
			
			$SQL = "update 	tblEventBus
					set		IsSending = 1
					where	EventId in ($eventList)";
			
			//Отметка о принятии событий
			$dal = DataAccess::GetConnection(Config::DB_EVENT_NAME());
			$dal->execute($SQL);
			DataAccess::CloseConnection($dal);
		}
		
		/**
		 * 
		 * Получает архив из директории с архивами
		 */
		static private function getArch() 
		{
			$archPath = Config::DB_ARCH_PATH();
			$dirs = scandir($archPath,1);
			
			foreach ($dirs as $file)
			{
				if($file === '.' || $file === '..')
					continue;
				
				if(strpos($file, 'DataArch') !== false)
				{
					//Получаем файл
					$result = gzfile($archPath.$file);
				}
				//Удаляем считанный архив
				unlink($archPath.$file);
				
				if(isset($result))
					break;
			}
			return $result;
		}
	}
?>