<?php
	/**
	 * 
	 * Управление crontab файлом
	 * @author D.Obrazcov
	 *
	 */
	class CronManager 
	{
		/**
		 * Создание crontab файла по устройствам
		 */
		public static function createCronTab()
		{
			$totalCron = ""; //Итоговый crontab	
			$autoStartSection = ""; // Набор адаптеров для автозапуска
			
			$totalCron = file_get_contents(Config::CRON_FILE_FLASH());
	
			if(($beginPos = strpos($totalCron, "#[Device crontab section]")) !== false)
				$totalCron = substr_replace($totalCron, '', $beginPos); 
	
			$totalCron .= "#[Device crontab section]";	
		
			$servicePath = ' -sp "'.realpath(dirname(__FILE__).'/../../adapterService/').'"';
			
			$deviceResult = DataHelper::executeQuery("select 
 												DeviceId
 												,Frequency
 												,AdapterName	 
											  from tblDeviceInfo D
											  where IsOn=1"
											 );
			foreach ($deviceResult as $row)
			{
				//Инициализация строк перед каждым новым устройством
				$addParam = ''; //Дополнительные аргументы запуска для устройства
				$section = ''; //Секция для crontab

				$argumentResult = DataHelper::executeQuery("select 
 													 ParamName
 													 ,Value
													from tblAdditionalArgument
													where DeviceId = ".$row['DeviceId']
											 		);
		
				//Формирование дополнительных параметров
				foreach ($argumentResult as $arg)
				{
					$addParam .= ' -a '.$arg['ParamName']." '".$arg['Value']."'";
				}
				
				// Создание списка адаптеров, запускаемых при старте системы
				$output = array();
				exec(Config::ADAPTERS_PATH().$row['AdapterName'].' -p ',$output);
				$adapterInfo = explode('|',trim($output[0],'|'));
				
				if(count($adapterInfo) > 3)
				{
					$isAutoRun = (string)trim($adapterInfo[3],' ');
					if($isAutoRun == '1')
					{
						$autoStartSection .= Config::ADAPTERS_PATH().$row['AdapterName'].' -r '.$row['DeviceId']
							.$servicePath.$addParam."\n";
					}
				}
				
				//Сохранение имеющейся секции
				$section = "\n#[DeviceId=".$row['DeviceId']." Freq=".$row['Frequency']."]";
				$section .= "\n*/".$row['Frequency'].' * * * * '.Config::ADAPTERS_PATH().$row['AdapterName'].' -r '.$row['DeviceId']
					.$servicePath.$addParam;
				//Добавление к общему файлу
				$totalCron .= $section;
			}

			try 
			{
				$fileName = Config::CRON_FILE_FLASH();
				$autoStartFile = Config::AUTORUN_FILE();
				//Очищаем файл
				unlink($fileName);
				unlink($autoStartFile);
				//Сохранение в файл
				$handle = fopen($fileName,'wb');
				fwrite($handle,$totalCron);
				fclose($handle);
				$handle = fopen($autoStartFile,'wb');
				fwrite($handle,$autoStartSection);
				fclose($handle);
				// Выставление прав на запуск	
				exec("chmod +x ".$autoStartFile);
				
				//Рестарт крона
				copy($fileName, Config::CRON_FILE());
				exec("killall -9 crond ; crond");
				 
			} catch (Exception $e) {}
		}
	}
?>