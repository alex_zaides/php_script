<?php
	
	/**
	 * 
	 * Менеджер архивирования БД
	 * @author D.Obrazcov
	 *
	 */
	class DBArchManager 
	{
		/**
		 * 
		 * Создание архивов
		 * @param принудительное создание архива $forceArch
		 */
		static public function ProceedArchive($forceArch=false)
		{
			try 
			{
				$fileName = Config::DB_DATA_NAME();
				//Получение размера файла БД
				$result = DataHelper::executeQueryOnDataDB("
																select count(*) * 45 as Size from tblParamData where ReadState = 0 OR ReadState = -1;
															");
				$fileSize = $result[0]["Size"];
				//Проверка размера
				$maxSize = Config::$DB_DATA_MAX;
				if(($fileSize < $maxSize) && !$forceArch)
				{
					return;
				}
	
				$packSize = Config::$PACKAGE_SIZE;
		
				//Получение данных
				$result = DataManager::GetData(true);
	
				$resultStr = ''; //Результат который будет сжиматься
				$archCount = 0; //Количество созданных архивов
				
				foreach ($result as $arr)
				{
					try 
						{
							$data = '';	
							$data .= $arr['ChannelId'].'|';
							$data .= $arr['ParamId'].'|';
							$data .= $arr['RegisterDate'].'|';
							$data .= $arr['Value'].'~';
							$resultStr .= $data;
							//Сжимается в 230 раз
							if((strlen($resultStr)/230) >= $packSize)
							{
								self::createArchive($resultStr);
								$archCount++;
								//Очистка			
								$resultStr = '';
							}
						} catch (Exception $e) {}
				}
				if($resultStr != '')
				{
					self::createArchive($resultStr);
					$archCount++;
				}
				//Увеличение счетчика архивов
				if($archCount > 0)
					DataHelper::executeNonQuery("update tblConfigSettings set Value = Value + ".$archCount." where Tag = 'ARCH_CNT'");
			}catch (Exception $e){}
		}
						
		static private function createArchive($resultStr)
		{
			$archPath = Config::DB_ARCH_PATH(); //Директория с архивами
			$ms = substr(microtime(),2,3);
			$date = getdate();
			//Формирование имени файла
			$archName = $archPath.'DataArch_'.$date['year'].'_'.$date['mon'].'_'.$date['mday'].'_'.$date['hours'].'_'.$date['minutes'].'_'. $date['seconds'].'_'.$ms.'.gz';
			//Архивирование порции данных
 			$arch = gzopen($archName,'w9');
 			gzwrite($arch, $resultStr);
 			gzclose($arch);
		}
	}
?>