<?php

	/**
	 * Менеджер работы с данными
	 * @author D.Obrazcov
	 *
	 */
	class DataManager 
	{
		/**
		 * 
		 * Считывание всех данных
		 */
		public static function GetData($withOrder = FALSE) 
		{
			try 
			{
				DataHelper::beginTransaction();
				$SQL = "
						select 
							ChannelId
							,ParamId
							,RegisterDate
							,Value
						from tblParamData 
						where	ReadState = 0";
				
				if($withOrder)
					$SQL .= " order by RegisterDate ASC;";
				else 
					$SQL .= ";";
				
				$result = DataHelper::execInTransactionQuery($SQL);
				
				DataHelper::execInTransactionNonQuery("update tblParamData set ReadState = 1 where ReadState = 0;");
				
				DataHelper::commitTransaction();
				
				return $result;
			
			} 
			catch (Exception $e) 
			{
				try 
				{
					DataHelper::rollBackTransaction();
				} 
				catch (Exception $err) {}
				
				LogManager::AddRecord($e->getMessage());
			}
		}
		
		/**
		 * 
		 * Сохранение массива данных
		 * @param Массив с данными $arrayOfData
		 */
		public static function SaveDataArray($arrayOfData)
		{
			if(!is_array($arrayOfData))
				exit;
			//Итоговая SQL строка
			$totalSQL = '';
			//Подготовка данных к записи	
			foreach ($arrayOfData as $paramData)
			{
				$totalSQL .= self::_CreateInsertStatement($paramData);
			}
			//Запись данных
			DataHelper::executeNonQueryOnDataDB($totalSQL);
		}
		
		/**
		 * 
		 * Создание SQL-выражения для вставки данных
		 * @param $data
		 */
		private static function _CreateInsertStatement($data)
		{
			if(!is_object($data))
				return '';
			$SQL = " INSERT INTO tblParamData (ChannelId, ParamId, RegisterDate, Value) VALUES (%d,%d,'%s',%f); ";
			return sprintf($SQL,
					$data->DataSrcId,
					$data->ParamId,
					$data->RegisterDate,
					$data->Value);
		}
	}
?>