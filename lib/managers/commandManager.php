<?php

	/**
	 * Класс управления командами
	 * @author dobrazcov
	 *
	 */
	class CommandManager 
	{
		/**
		 * Сохранение массива команд
		 * @param array $commandList
		 */
		public static function saveCommands($commandList) 
		{
			$totalSQL = '';
			
			foreach ($commandList as $command)
			{
				$totalSQL .= self::_getInsertStatement($command);
			}
			
			// Сохранение на флешку
			DataHelper::executeNonQueryOnEventDB($totalSQL);
		}
		
		/**
		 * Получение статусов команд
		 * @param string $idList
		 * @param string $source
		 */
		public static function getCommandState($idList, $source)
		{
			$SQL = "select 
						CommandId
						,State
						,ifnull(Result,'') as Result
						,ifnull(EndExecDate,'') as EndExecDate
					from tblCommand
					where CommandId in (%s) and Source = '%s';
					";
			
			$SQL = sprintf($SQL,$idList,$source);
			
			$dbResult = DataHelper::executeQueryOnEventDB($SQL,true);
			
			return $dbResult;
		}
		
		/**
		 * Получение еще не обработанных команд
		 */
		public static function getNotProceededCommand() 
		{
			$SQL = "select
							CommandId
							,Source
							,State
							,Type
							,Message
							,DeviceId
						from tblCommand
						where State in (5,6)
						order by Priority,ArrivalDate DESC;";
			
			$dbResult = DataHelper::executeQueryOnEventDB($SQL);
			
			$commandList = array();
			
			foreach ($dbResult as $row)
			{
				$command = new Command();
				$command->commandId = $row["CommandId"];
				$command->source = $row["Source"];
				$command->state = $row["State"];
				$command->type = $row["Type"];
				$command->message = $row["Message"];
				$command->deviceId = $row["DeviceId"];
				
				array_push($commandList, $command);
			}
			
			return $commandList;
		} 
		
		/**
		 * Обновление состояния команды
		 * @param Command $command
		 */
		public static function updateState(Command $command) 
		{
			$SQL = "update tblCommand
					set	State = %d
						,Result = '%s'
						,EndExecDate = '%s'
					where	CommandId = %d";
			$SQL = sprintf($SQL,$command->state,$command->result,$command->endExecDate,$command->commandId);
			
			//Сохранение на флешку
			DataHelper::executeNonQueryOnEventDB($SQL);
		}
		
		/**
		 * Удаление переданных команд
		 * @param string $commandList
		 * @param string $source
		 */
		public static function removeCommand($commandList, $source)
		{
			$SQL = "delete from tblCommand
					where CommandId in (%s) and Source = '%s';";
			
			$SQL = sprintf($SQL,$commandList,$source);
			
			// Удаление с флешки
			DataHelper::executeNonQueryOnEventDB($SQL);
		}
		
		/**
		 * Получение инструкции для вставки команды
		 * @param Command $command
		 */
		private static function _getInsertStatement(Command $command) 
		{
			$SQL = "insert into tblCommand (CommandId,Source,State,Type,Message,Priority,DeviceId,ArrivalDate)
					values (%d,'%s',%d,%d,'%s',%d,%d,'%s');";
			
			return sprintf($SQL, 
								$command->commandId,
								$command->source,
								$command->state,
								$command->type,
								$command->message,
								$command->priority,
								$command->deviceId,
								$command->arrivalDate
								);
		}
	}

?>