<?php

	// менеджер адаптеров 
	class AdapterManger
	{
		public static  function getAdaptersList()
		{
			$result = array();
			$output = array();
			$dirName = Config::ADAPTERS_PATH();
			
			if ($handle = opendir($dirName))
			{
				//Перебор адаптеров
    			while (false !== ($entry = readdir($handle))) 
    			{
        			if ($entry != "." && $entry != "..") 
        			{
        				try 
        				{
        					$adapter = new Adapter();
        					$adapter->FileName = $entry;
	            			//Обращение к адаптеру
	            			$command = $dirName.$adapter->FileName." -p";
	            			unset($output);
	            			
	            			//Получение информации по адаптеру и параметрам
	            			exec($command,$output);
	            			$adapter->AdapterInfo = $output[0];
	            			
							for ((int) $i = 1; $i < count($output); $i++) 
							{
								$adapter->ParamInfo .= ';;'.$output[$i];
							}
							
							$adapter->ParamInfo = trim($adapter->ParamInfo,';;');
							
							//Получение информации по аргументам запуска
							$command = $dirName.$adapter->FileName." -a";
							unset($output);
							exec($command,$output);
							
							for ((int) $i = 0; $i < count($output); $i++) 
							{
								$adapter->AdditionalArgument .= ';;'.$output[$i];
							}
							
							$adapter->AdditionalArgument = trim($adapter->AdditionalArgument,';;');
	            															
        					array_push($result, $adapter);
	            				
        				} catch (Exception $e) 
        				{
        					continue;
        				}
        			}
    			}
    			closedir($handle);
			}
			
			return $result;
		}
	} 
?>