<?php
	/**
	 * 
	 * Менеджер управления событиями
	 * @author D.Obrazcov
	 *
	 */
	class EventManager 
	{
		/**
		 * 
		 * Получение массива с событиями
		 */
		/*public static function getEvents() 
		{
			$result = array();
			$SQL = "select * from tblEventBus order by EventId ASC";
			//Получение данных
			$dbResult = DataHelper::executeQueryOnEventDB($SQL);
			//Обработка результатов
			foreach($dbResult as $row)
			{
				$event = new Event();
				$event->EventId = $row['EventId'];
				$event->TypeId = $row['TypeId'];
				$event->DataSrcId = $row['DataSrcId'];
				$event->RegisterDate = $row['RegisterDate'];
				$event->Argument1 = $row['Argument1'];
				$event->Argument2 = $row['Argument2'];
				$event->Argument3 = $row['Argument3'];
				$event->Argument4 = $row['Argument4'];
				
				array_push($result,$event);
			}
			return $result;
		}*/
		
		/**
		 * 
		 * Сохранение массива событий
		 * @param массив с событиями $eventList
		 */
		public static function saveEvents($eventList)
		{
			if(!is_array($eventList))
				exit;
			
			//Итоговый запрос
			$totalSQL = ''; 
			//Подготовка запроса
			foreach ($eventList as $event)
			{
				$totalSQL .= self::createInsertStatement($event);
			}
			
			//Сохранение событий
			DataHelper::executeNonQueryOnEventDB($totalSQL);
		}  
		
		/**
		 * 
		 * Получение SQL-выражения для вставки данных
		 * @param событие $event
		 */
		private static function createInsertStatement($event)
		{
			if(!is_object($event))
				exit;
			//Подготавливаем скрипт
			$SQL = "insert into tblEventBus
					(
						TypeId
						,ChannelId
						,RegisterDate
						,Argument1
						,Argument2
						,Argument3
						,Argument4
					)
					values (%d,%d,'%s','%s','%s','%s','%s'); ";
			return sprintf($SQL,
								$event->TypeId
								,$event->DataSrcId
								,$event->RegisterDate
								,$event->Argument1
								,$event->Argument2
								,$event->Argument3
								,$event->Argument4
								);
		}
	}
?>