<?php

	/**
	 * Менеджер сенсоров
	 * @author dobrazcov
	 *
	 */
	class SensorManager
	{
		/**
		 * Получить список сенсоров устройства
		 * @param int $deviceID
		 */
		public static function getSensorList($deviceID)
		{
			$ret=array();
			
			$SQL = "SELECT * FROM tblSensorInfo WHERE DeviceId = %d";
			$SQL = sprintf($SQL,$deviceID);
			$result = DataHelper::executeQuery($SQL);
			
			foreach ($result as $arr)
			{
				$sensor=new SensorInfo();
				$sensor->DeviceId=$arr['DeviceId'];
				$sensor->SensorId=$arr['SensorId'];
				$sensor->Address=$arr['SensorAddress'];
				$sensor->SerialKey=$arr['SerialKey'];
				
				array_push($ret, $sensor);
			}
			return $ret;
		}
		
		/**
		 * Сохранить сенсор
		 * @param SensorInfo $sensor
		 */
		public static function saveSensor($sensor)
		{
			$SQL = "update tblSensorInfo set SensorAddress = '%s', DeviceId=%d, SerialKey='%s' where SensorId=%d";
			
			$SQL = sprintf($SQL
							,$sensor->Address
							,$sensor->DeviceId
							,$sensor->SerialKey
							,$sensor->SensorId);
			
			DataHelper::executeNonQuery($SQL);
		}
		
		/**
		 * Добавить сенсор
		 * @param SensorInfo $sensor
		 */
		public static function insertSensor($sensor) 
		{
			$SQL = "insert into tblSensorInfo (SensorAddress, DeviceId, SerialKey)
					values ('%s',%d,'%s'); ";
			
			$SQL = sprintf($SQL
							,$sensor->Address
							,$sensor->DeviceId
							,$sensor->SerialKey);
			
			DataHelper::executeNonQuery($SQL);
			
			$result = DataHelper::executeQuery("select seq from sqlite_sequence where name = 'tblSensorInfo';");
			
			return $result[0]["seq"];
		}
		
		/**
		 * Удаление сенсора
		 * @param int $id
		 */
		public static function removeSensor($id)
		{
			$SQL = " delete from tblSensorInfo where SensorId=".$id." ;
				   delete from tblChanelInfo where SensorId=".$id." ;
				   delete from tblEventLimValue 
				   where rowid in 
						(
							select E.rowid 
							from tblEventLimValue E
								left join tblChanelInfo C on (C.ChanelId = E.ChannelId)
							where
								C.ChanelId is null
						); ";
						
			DataHelper::executeNonQuery($SQL);
		}
	} 
?>