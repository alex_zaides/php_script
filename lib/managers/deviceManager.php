<?php
	/**
	 * Менеджер устройств
	 * @author dobrazcov
	 */
	class DeviceManager
	{
		/**
		 * Получение всех устройств контроллера
		 */
		public static function getDevices($withSensors)
		{
			$SQL = "SELECT * FROM tblDeviceInfo";
			$SensorSQL = "Select * from tblSensorInfo where DeviceId = '%d'";
			
			$result = DataHelper::executeQuery($SQL);
			
			$deviceList = array();
			
			foreach ($result as $arr)
			{
				$device = new Device();
				$device->DeviceId=$arr['DeviceId'];
				$device->Address=$arr['Address'];
				$device->Frequency=$arr['Frequency'];
				$device->AdapterName=$arr['AdapterName'];
				$device->IsOn=$arr['IsOn'];

				if($withSensors)
				{
					$device->Sensors = SensorManager::getSensorList($device->DeviceId);
				}
				
				$device->Arguments = AdditionalArgumentManager::getArguments($device->DeviceId);
				
				array_push($deviceList, $device);
			}
			return $deviceList;
		}
		
		/**
		 * Сохранение устройства
		 * @param Device $device
		 */
		public static function saveDevice($device) 
		{
			$SQL = "update tblDeviceInfo set Address = '%s', AdapterName = '%s', Frequency = %d, IsOn = %d where DeviceId = %d ";
			$SQL = sprintf($SQL
							,$device->Address
							,$device->AdapterName
							,$device->Frequency
							,$device->IsOn
							,$device->DeviceId);
							
			DataHelper::executeNonQuery($SQL);
		}
		
		/**
		 * Добавление нового объекта
		 * @param Device $device
		 */
		public static function insertDevice($device)
		{
			$SQL = "insert into tblDeviceInfo (Address, AdapterName, Frequency)
					values ('%s','%s',%d);";
			
			$SQL = sprintf($SQL
							,$device->Address
							,$device->AdapterName
							,$device->Frequency);
							
			DataHelper::executeNonQuery($SQL);
			$result = DataHelper::executeQuery("select seq from sqlite_sequence where name = 'tblDeviceInfo';");
			
			return $result[0]["seq"];
		}
		
		/**
		 * Удаление устройства
		 * @param int $id
		 */
		public static function removeDevice($id)
		{
			$SQL = "delete from tblDeviceInfo where DeviceId=".$id.";
				   delete from tblAdditionalArgument where DeviceId=".$id." ;
				   delete from tblSensorInfo where DeviceId=".$id." ;
				   delete from tblChanelInfo where DeviceId=".$id." ;
				   delete from tblEventLimValue 
				   where rowid in 
						(
							select E.rowid 
							from tblEventLimValue E
								left join tblChanelInfo C on (C.ChanelId = E.ChannelId)
							where
								C.ChanelId is null
						); ";
						
			DataHelper::executeNonQuery($SQL);
		}
		
		/**
		 * Сохранение атрибутов устройств
		 * @param array $attrList
		 */
		public static function saveAttributes($attrList)
		{
			//Чистим от старых значений
			$deleteSQL = 'delete from tblAttribute';
			DataHelper::executeNonQueryOnEventDB($deleteSQL);
						
			$totalSQL = '';
				
			foreach ($attrList as $attr)
			{
				$totalSQL .= self::_getInsertStatement($attr);
			}
				
			// Сохранение на флешку
			DataHelper::executeNonQueryOnEventDB($totalSQL);
		}
		
		
		public static function changeActive($id, $IsOn)
		{
			$SQL = "update tblDeviceInfo set IsOn = %d where DeviceId = %d ";
			$SQL = sprintf($SQL
							,$IsOn
							,$id);
							
			DataHelper::executeNonQuery($SQL);
		}
		/**
		 * Получение инструкции для вставки атрибута
		 * @param Attribute $attr
		 */
		private static function _getInsertStatement(Attribute $attr)
		{
			$SQL = "insert into tblAttribute (DeviceId,AttrId,Value)
					values (%d,'%d','%s');";
				
			return sprintf($SQL,$attr->deviceId,$attr->attrId,$attr->value);
		}
	}
?>