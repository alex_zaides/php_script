<?php
	/**
	 * 
	 * Менеджер каналов и их настроек
	 * @author dobrazcov
	 *
	 */
	class HolidayManager 
	{
		public static function SaveHoliday($holidays)
		{
			$totalSQL = "delete from tblEventHolyday  where substr(Days, 1, 4)='%s';";
			$totalSQL = sprintf($totalSQL, $holidays->Year);
			$SQLIns	= "insert into tblEventHolyday (Days) values (%s);";
			foreach($holidays->Days as $holiday)
			{
				$totalSQL .= sprintf($SQLIns, ($holidays->Year.$holiday->Days));
			}		

			DataHelper::executeNonQuery($totalSQL);		
		}
		public static function GetHolidays($Year)
		{
			$Days = new Holidays($Year);
			$SQL = "select Days from tblEventHolyday where substr(Days, 1, 4)='%s';";
			$SQL = sprintf($SQL, $Year); 
			$valuesResult = DataHelper::executeQuery($SQL );
			foreach ($valuesResult as $valueRow){
				$day = new Holiday();	
				$day->Days = substr($valueRow['Days'], 4);
				array_push($Days->Days,$day);
			}
			return $Days;
		}
	}