<?php
	
	/**
	 * 
	 * Менеджер параметров запуска устройства
	 * @author dobrazcov
	 *
	 */
	class AdditionalArgumentManager
	{
		/**
		 * Получение аргументов устройства
		 * @param int $deviceId
		 */
		public static function getArguments($deviceId)
		{
			$SQL = "select
						DeviceId
						,ParamName
						,Value
					from tblAdditionalArgument
					where DeviceId = ".$deviceId;
			
			$result = DataHelper::executeQuery($SQL);

			$argList = array();
			
			foreach ($result as $arr)
			{
				$arg = new AdditionalArgument();
				$arg->DeviceId = $arr['DeviceId'];
				$arg->Name = $arr['ParamName'];
				$arg->Value = $arr['Value'];
				
				array_push($argList, $arg);
			}
			return $argList;
		}
		
		/**
		 * Сохранение аргументов запуска
		 * @param array $list
		 * @param int $deviceId
		 */
		public static function updateArguments($list,$deviceId) 
		{
			$SQL = "delete from tblAdditionalArgument where DeviceId = %d; ";
			$SQL = sprintf($SQL,$deviceId);
			
			DataHelper::executeNonQuery($SQL);
						
			$totalSQL = '';
			$SQL = " insert into tblAdditionalArgument (DeviceId, ParamName, Value)
					values (%d, '%s', '%s'); ";
			
			foreach($list as $arg)
			{
				$totalSQL .= sprintf($SQL,$deviceId,$arg->Name,$arg->Value);
			}
			
			DataHelper::executeNonQuery($totalSQL);
		}
		
		/**
		 * Удаление всех аргументов не содержащихся в списке
		 * @param string $listId
		 */
		public static function removeArgument($listId)
		{
			if($listId == '')
				return;
				
			$SQL = "delete from tblAdditionalArgument where DeviceId not in (".$listId."); ";
						
			DataHelper::executeNonQuery($SQL);
		}
	}
?>