<?php
	/**
	 * Менеджер управления контроллером
	 * @author dobrazcov
	 *
	 */
	class ControllerManger
	{ 
		/**
		 * Получить контроллер
		 * @param bool $withDevices
		 */
		public static function getController($withDevices=true)
		{
			$result = DataHelper::executeQuery("SELECT * FROM tblControllerInfo");
		    $arr = $result[0];
		    
			$controller = new Controler();
			$controller->ControllerID=$arr['ControllerId'];
			$controller->IP=$arr['IP'];
			$controller->MAC=$arr['MAC'];
			$controller->NetPriority=$arr['NETPriority'];
			$controller->Address=$arr['Address'];
			$controller->TimeZone=$arr['TimeZone'];
			$controller->DNS1=$arr['DNS1'];
			$controller->DNS2=$arr['DNS2'];
			$controller->NetMask=$arr['NetMask'];
			$controller->Gateway=$arr['Gateway'];
			$controller->NTPServer=$arr['NTPServer'];
			$controller->UsePPP=$arr['UsePPP'];
			$controller->UsePPP2=$arr['UsePPP2'];
			$controller->PPPLogin=$arr['PPPLogin'];
			$controller->PPPLogin2=$arr['PPPLogin2'];
			$controller->PPPPass=$arr['PPPPass'];
			$controller->PPPPass2=$arr['PPPPass2'];
			$controller->PPPSerial=$arr['PPPSerial'];
			$controller->PPPBaud=$arr['PPPBaud'];
			$controller->APN=$arr['APN'];
			$controller->APN2=$arr['APN2'];
			$controller->CheckServerName=$arr['CheckServerName'];
			$controller->UseVPN=$arr['UseVPN'];
			$controller->VPNType=$arr['VPNType'];
			$controller->VPNLogin=$arr['VPNLogin'];
			$controller->VPNLogin2=$arr['VPNLogin2'];
			$controller->VPNPWD=$arr['VPNPWD'];
			$controller->VPNPWD2=$arr['VPNPWD2'];
			$controller->VPNKeyFileName=$arr['VPNKeyFileName'];
			$controller->VPNConnectServer=$arr['VPNConnectServer'];
			$controller->VPNConnectServer2=$arr['VPNConnectServer2'];
			$controller->VPNPort=$arr['VPNPort'];
			$controller->VPNPort2=$arr['VPNPort2'];
			$controller->HostService=$arr['HostService'];
			$controller->ReInit=$arr['ReInitialized'];
			$controller->UsePPPoE=$arr['UsePPPoE'];
			$controller->PPPoEType=$arr['PPPoEType'];
			$controller->PPPoEServer=$arr['PPPoEServer'];
			$controller->PPPoELogin=$arr['PPPoELogin'];
			$controller->PPPoEPass=$arr['PPPoEPass'];
						
			if($withDevices)
			{
				$controller->Devices = DeviceManager::getDevices(true);
			}
			return $controller;
		}
			
		/**
		 * Сохранить сетевые настройки
		 * @param Controler $controller
		 */ 
		public static function saveNetworkSettings(&$controller)
		{
			$totalSQL="";
			
			$SQL = " UPDATE tblControllerInfo 
					 SET IP='%s',
					 	MAC='%s',
					 	NETPriority='%s',
						DNS1='%s',
						DNS2='%s',
						NetMask='%s',
						Gateway='%s',
						NTPServer='%s',
						UsePPP='%d',
						UsePPP2='%d',
						PPPLogin='%s',
						PPPLogin2='%s',
						PPPPass='%s',
						PPPPass2='%s',
						PPPSerial='%s',
						PPPBaud='%s',
						APN='%s',
						APN2='%s',
						CheckServerName='%s',
						UseVPN='%d',
						VPNType='%s',
						VPNLogin='%s',
						VPNLogin2='%s',
						VPNPWD='%s',
						VPNPWD2='%s',
						VPNKeyFileName='%s',
						VPNConnectServer='%s',
						VPNConnectServer2='%s',
						VPNPort='%d',
						VPNPort2='%d',
						HostService='%s',
						UsePPPoE='%d',
						PPPoEType='%s',
						PPPoEServer='%s',
						PPPoELogin='%s',
						PPPoEPass='%s'
					";
			
			$SQL=sprintf($SQL,$controller->IP,
							$controller->MAC,
							$controller->NetPriority,
							$controller->DNS1,
							$controller->DNS2,
							$controller->NetMask,
							$controller->Gateway,
							$controller->NTPServer,
							$controller->UsePPP,
							$controller->UsePPP2,
							$controller->PPPLogin,
							$controller->PPPLogin2,
							$controller->PPPPass,
							$controller->PPPPass2,
							$controller->PPPSerial,
							$controller->PPPBaud,
							$controller->APN,
							$controller->APN2,
							$controller->CheckServerName,
							$controller->UseVPN,
							$controller->VPNType,
							$controller->VPNLogin,
							$controller->VPNLogin,
							$controller->VPNPWD,
							$controller->VPNPWD,
							$controller->VPNKeyFileName,
							$controller->VPNConnectServer,
							$controller->VPNConnectServer2,
							$controller->VPNPort,
							$controller->VPNPort2,
							$controller->HostService,
							$controller->UsePPPoE,
							$controller->PPPoEType,
							$controller->PPPoEServer,
							$controller->PPPoELogin,
							$controller->PPPoEPass
							);

			$totalSQL=$totalSQL.$SQL;
			DataHelper::executeNonQuery($SQL);
			return  $controller;
		}
		
		/**
		 * Сохранить контроллер
		 * @param Controler $controller
		 */
		public static function saveController($controller)
		{
			$SQL = "UPDATE tblControllerInfo set TimeZone=%d, Address='%s'";
			$SQL = sprintf($SQL,$controller->TimeZone,$controller->Address);
			
			DataHelper::executeNonQuery($SQL);
		}
		
		/**
		 * Переинициализировать контроллер
		 */
		public static function reinitController()
		{
			$SQL = "update tblControllerInfo set ReInitialized = 1;";
			DataHelper::executeNonQuery($SQL);
		}
		
		/**
		 * Перевод таблицы tblControllerInfo на новую версию сетевых настроек
		 */
		public static function upgradeControllerTable() 
		{
			$checkColumn = 'UsePPP2'; //Колонка для проверки версии
			$SQL = "select $checkColumn from tblControllerInfo";
			$result = DataHelper::executeQuery($SQL);
			
			$upgrade = !(isset($res[0]) && isset($res[0][$checkColumn]));
			
			if($upgrade)
			{
				//Upgrade таблицы tblControllerInfo
				$upgradeSQL = 'alter table "tblControllerInfo" add "UsePPPoE" integer NOT NULL DEFAULT \'0\';  
  								alter table "tblControllerInfo" add "PPPoEType" text NULL;
  								alter table "tblControllerInfo" add "PPPoELogin" text NULL;
  								alter table "tblControllerInfo" add "PPPoEPass" text NULL;
  								alter table "tblControllerInfo" add "PPPoEServer" text NULL;
  								alter table "tblControllerInfo" add "UsePPP2" integer NOT NULL DEFAULT \'0\';
  								alter table "tblControllerInfo" add "PPPLogin2" text NULL;
  								alter table "tblControllerInfo" add "PPPPass2" text NULL;
  								alter table "tblControllerInfo" add "APN2" text NULL;
  								alter table "tblControllerInfo" add "VPNLogin2" text NULL;
  								alter table "tblControllerInfo" add "VPNPWD2" text NULL;
								alter table "tblControllerInfo" add "VPNConnectServer2" text NULL;
  								alter table "tblControllerInfo" add "VPNPort2" integer NULL;';
  				DataHelper::executeNonQuery($upgradeSQL);
			}
		}
	}
?>