<?php

	/**
	 * Менеджер работы с лог файлом
	 * @author D.Obrazcov
	 *
	 */
	class LogManager 
	{
		/**
		 * Информационное сообщение
		 * @var string
		 */
		const INFO_LOG = 'Info';
		
		/**
		 * Сообщение об ошибке
		 * @var string
		 */
		const ERROR_LOG = 'Error';

		/**
		 *
		 * Добавление новой записи в системный лог
		 * @param $message Сообщение
		 * @param $messageType Тип сообщения
		 */
		public static function AddRecord($message,$messageType = self::ERROR_LOG)
		{
			if($message == null || $message == '')
				return;
		
			$message = '['.$messageType.'] '.$message;
		
			try
			{
				if($messageType == self::ERROR_LOG)
					$priority = LOG_ERR;
				else 
					$priority = LOG_INFO;
				syslog($priority, $message);
			}
			catch (Exception $e){}
		}
		
		
		/**
		 * 
		 * Добавление новой записи в лог файл
		 * @param $message Сообщение
		 * @param $messageType Тип сообщения
		 */
		public static function AddRecordToFile($message,$messageType = self::ERROR_LOG) 
		{
			if($message == null || $message == '')
				return;

			//Добавление даты и времени записи
			$date = Config::GET_DATE();
			$message = "\r\n".$messageType.' ['.$date.'] '.$message;
						
			$filename = Config::LOG_FILE_PATH();
			
			try 
			{
				//Открытие файла
	    		if ($handle = fopen($filename, 'a')) 
	    		{
	    			fwrite($handle, $message);
	    			fclose($handle);
	    		}
			}
			catch (Exception $e){}
		}
		
		/**
		 * 
		 * Очистка лог файла
		 */
		public static function ClearLog()
		{
			$file = Config::LOG_FILE_PATH();
			if($handle = fopen($file,'w'))
			{
				try 
				{
					fwrite($handle,'');
				} 
				catch (Exception $e) {}
				fclose($handle);
			}
		} 
	}
?>