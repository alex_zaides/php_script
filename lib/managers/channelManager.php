<?php
	/**
	 * 
	 * Менеджер каналов и их настроек
	 * @author dobrazcov
	 *
	 */
	class ChannelManager 
	{

		public static function fillChannel($dbResult,$deviceId=-1)
		{	
			$isFirst = true;
			$adapterResult = array();
			unset($adapterResult);
			$adapterSetting = array();
			unset($adapterSetting);
			$list = array();
			foreach ($dbResult as $row)
			{
				if($isFirst){
					$isFirst = false;
					$execFile = Config::ADAPTERS_PATH().$row['AdapterName'].' -s';
					exec($execFile,$adapterResult);
					$doc = simplexml_load_string($adapterResult[0]);
					$adapterSetting=$doc[0];
				}				
				$channel = new ChanelInfo();
				$channel->ChanelId = $row['ChanelId'];
				$channel->SerialKey = (($deviceId!=-1)?($deviceId.'_'.$row['SensorKey'].'_'):'').$row['SerialKey'];
				$channel->Address = $row['Address'];
				$channel->ParamId = $row['ParamId'];
				$channel->InterfaceArgument1 = $row['InterfaceArgument1'];
				$channel->InterfaceArgument2 = $row['InterfaceArgument2'];
				$channel->InterfaceArgument3 = $row['InterfaceArgument3'];
				$channel->InterfaceArgument4 = $row['InterfaceArgument4'];
				$channel->IsEvent = (boolean)$row['IsEvent'];
				$channel->IsOn = (boolean) $row['IsOn'];
				if((1==1 || $channel->IsEvent )&&isset($adapterSetting))
				{
					foreach($adapterSetting->EventSettingType as $setting)
					{
						if($setting['Param'] == $row['ParamId']){
							$eventLim = new EventLimType();	
							$eventLim->Id = $setting['Param'];
							$eventLim->Type = $setting['Type'];
							$eventLim->Tag = '';
							array_push($channel->EventLims,$eventLim);
							$eventSQL = "select IntervalId, StartTime, EndTime, Params, IsHoliday from tblEventLimInterval 
										 where LimId = ".$setting['Type']. "  and ChannelId = ".$row['ChanelId'];
							$valuesResult = DataHelper::executeQuery($eventSQL);
							foreach ($valuesResult as $valueRow)
							{
								$valueLim = new EventLimValue();	
								$valueLim->Id     = $valueRow['IntervalId'];
								$valueLim->Type   = $setting['Type'];;
								$valueLim->Start  = $valueRow['StartTime'];
								$valueLim->Finish = $valueRow['EndTime'];
								$valueLim->Value  = $valueRow['Params'];
								$valueLim->IsHoliday  	= $valueRow['IsHoliday'];
								array_push($channel->EventVals,$valueLim);
							}
						}
					}
				}
				array_push($list,$channel);
			}
			return $list;
		}
		/**
		 * Получение каналa устройства
		 */
		public static function getChannelById($chanelId) 
		{
			//Формирование запроса для получения настроек канала
			$sqlStatement = "select
								C.ChanelId
								,ifnull(S.SerialKey,'X') as SensorKey	
								,C.SerialKey as SerialKey
								,C.Address
								,case 
									when OverridenParamId = -1 then ParamId
									else OverridenParamId
								end as ParamId
								,ifnull(case InterfaceArgument1 when '' then null else InterfaceArgument1 end,'X') as InterfaceArgument1
								,ifnull(case InterfaceArgument2 when '' then null else InterfaceArgument2 end,'X') as InterfaceArgument2
								,ifnull(case InterfaceArgument3 when '' then null else InterfaceArgument3 end,'X') as InterfaceArgument3
								,ifnull(case InterfaceArgument4 when '' then null else InterfaceArgument4 end,'X') as InterfaceArgument4
								,C.IsOn
								,IsEvent
								,d.AdapterName
							from tblChanelInfo C
							join tblDeviceInfo d on d.DeviceId = c.DeviceId
							left join tblSensorInfo S on (S.SensorId = C.SensorId)
							where C.ChanelId = ".$chanelId.";";
			//Выполнение скрипта
			$dbResult = DataHelper::executeQuery($sqlStatement);
		
			//Обработка результатов
			return self::fillChannel($dbResult);
		}
		/**
		 * Получение каналов устройства
		 * @param $deviceId Идентификатор устройства
		 * @param $onlyDevice Получение каналов только по девайсу (без сенсоров)
		 * @param $onlyOn Получение только включенных каналов
		 */
		public static function getChannelByDeviceId($deviceId, $onlyDevice = false, $onlyOn = true) 
		{
			$channelList = array();
			//Формирование запроса для получения настроек канала
			$sqlStatement = "select
								C.ChanelId
								,ifnull(S.SerialKey,'X') as SensorKey	
								,C.SerialKey as SerialKey
								,C.Address
								,case 
									when OverridenParamId = -1 then ParamId
									else OverridenParamId
								end as ParamId
								,ifnull(case InterfaceArgument1 when '' then null else InterfaceArgument1 end,'X') as InterfaceArgument1
								,ifnull(case InterfaceArgument2 when '' then null else InterfaceArgument2 end,'X') as InterfaceArgument2
								,ifnull(case InterfaceArgument3 when '' then null else InterfaceArgument3 end,'X') as InterfaceArgument3
								,ifnull(case InterfaceArgument4 when '' then null else InterfaceArgument4 end,'X') as InterfaceArgument4
								,C.IsOn
								,IsEvent
								,d.AdapterName
							from tblChanelInfo C
								 left join tblSensorInfo S on (S.SensorId = C.SensorId)
								 join tblDeviceInfo d on d.DeviceId = c.DeviceId
							where C.DeviceId = ".$deviceId;
			if($onlyOn)
				$sqlStatement .= " and IsOn = 1";
			if($onlyDevice)
				$sqlStatement .= " and SensorId = -1";
			$sqlStatement .= ';';
			//Выполнение скрипта
			$dbResult = DataHelper::executeQuery($sqlStatement);
			//Обработка результатов
			return self::fillChannel($dbResult, $deviceId);
		}
		
		/**
		 * Получение всех каналов предка
		 * @param int $parentId
		 * @param int $isDevice
		 */
		public static function getChannelByParentId($parentId, $isDevice)
		{
			$SQL = "select 
						c.ChanelId
						,c.SerialKey
						,c.Address
						,c.ParamId
						,c.OverridenParamId
						,c.DeviceId
						,c.SensorId
						,c.IsEvent
						,c.IsOn
						,c.InterfaceArgument1
						,c.InterfaceArgument2
						,c.InterfaceArgument3
						,c.InterfaceArgument4
						,d.AdapterName
					from tblChanelInfo c
					join tblDeviceInfo d on d.DeviceId = c.DeviceId";
			if($isDevice == 1)
				$SQL .= " where c.DeviceId = ".$parentId." and c.SensorId = -1;";
			else 
				$SQL .= " where c.SensorId = ".$parentId;

			$list = array();
			$result = DataHelper::executeQuery($SQL);
			return self::fillChannel($result);
		}

		/**
		 * Сохранение канала
		 * @param ChanelInfo $channel
		 */
		public static function saveChannel($channel)
		{
			$SQL = "update tblChanelInfo
					set	SensorId = %d
						,ParamId = %d
						,Address = '%s'
						,DeviceId = %d
						,IsEvent = %d
						,OverridenParamId = %d
						,InterfaceArgument1 = '%s'
						,InterfaceArgument2 = '%s'
						,InterfaceArgument3 = '%s'
						,InterfaceArgument4 = '%s'
						,IsOn = %d 
						,SerialKey = '%s'
					where ChanelId = %d";
			
			$SQL = sprintf($SQL,
								$channel->SensorId
								,$channel->ParamId
								,$channel->Address
								,$channel->DeviceId
								,$channel->IsEvent
								,$channel->OverridenParamId
								,$channel->InterfaceArgument1
								,$channel->InterfaceArgument2
								,$channel->InterfaceArgument3
								,$channel->InterfaceArgument4
								,$channel->IsOn
								,$channel->SerialKey
								,$channel->ChanelId
							);
							
			DataHelper::executeNonQuery($SQL);
		}
		
		/**
		 * Вставка нового канала
		 * @param ChanelInfo $channel
		 */
		public static function insertChannel($channel)
		{
			$SQL = "insert into tblChanelInfo
					(SensorId, ParamId, Address, DeviceId, IsEvent, OverridenParamId, InterfaceArgument1, InterfaceArgument2, InterfaceArgument3, InterfaceArgument4, IsOn, SerialKey)
					values (%d,%d,'%s',%d,%d,%d,'%s','%s','%s','%s',%d,'%s')";
			
			$SQL = sprintf($SQL,
								$channel->SensorId
								,$channel->ParamId
								,$channel->Address
								,$channel->DeviceId
								,$channel->IsEvent
								,$channel->OverridenParamId
								,$channel->InterfaceArgument1
								,$channel->InterfaceArgument2
								,$channel->InterfaceArgument3
								,$channel->InterfaceArgument4
								,$channel->IsOn
								,$channel->SerialKey
							);
							
			DataHelper::executeNonQuery($SQL);
			
			$result = DataHelper::executeQuery("select seq from sqlite_sequence where name = 'tblChanelInfo'");
			
			return $result[0]["seq"];
		}
		
		/**
		 * Обновление границ возникновения событий
		 * @param array $list
		 * @param int $channelId
		 */
		public static function updateEventLim($channelId, $intervallist)
		{
			$totalSQL = "";
			$SQLDel = "delete from tblEventLimInterval where ChannelId=%d ;";
			$totalSQL .= sprintf($SQLDel, $channelId);
			$SQLIns	= "insert into tblEventLimInterval (ChannelId, LimId, StartTime, EndTime, Days, Params, IsHoliday) values
																(%d, %d, '%s', '%s', 1, '%s', %d);";
			foreach($intervallist as $eventLim)
			{
				$totalSQL .= sprintf($SQLIns, $channelId, $eventLim->Type, $eventLim->Start, $eventLim->Finish, $eventLim->Value, $eventLim->IsHoliday);
			}
			DataHelper::executeNonQuery($totalSQL);
		}
		
		/**
		 * Удаление Канала
		 * @param int $id
		 */
		public static function removeChannel($id)
		{
			$SQL = " delete from tblChanelInfo where ChanelId=".$id." ;
				   delete from tblEventLimValue 
				   where rowid in 
						(
							select E.rowid 
							from tblEventLimValue E
								left join tblChanelInfo C on (C.ChanelId = E.ChannelId)
							where
								C.ChanelId is null
						); ";
						
			DataHelper::executeNonQuery($SQL);
		}
	}
?>