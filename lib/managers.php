<?php
	require_once 'managers/controllerManager.php';
	require_once 'managers/adapterManager.php';
	require_once 'managers/deviceManager.php';
	require_once 'managers/additionalArgumentManager.php';
	require_once 'managers/userManager.php';
	require_once 'managers/sensorManager.php';
	require_once 'managers/logManager.php';
	require_once 'managers/dataManager.php';
	require_once 'managers/dbArchManager.php';
	require_once 'managers/collectorManager.php';
	require_once 'managers/eventManager.php';
	require_once 'managers/cronManager.php';
	require_once 'managers/channelManager.php';
	require_once 'managers/commandManager.php';
	require_once 'managers/holidayManager.php';
?>