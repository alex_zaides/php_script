#!/usr/bin/php
<?php

	/**
	 * @deprecated (Переход на работу через dbgateway)
	 */

	//Прием данным от адаптеров
	//D.Obrazcov
	
	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	register_shutdown_function('DataHelper::abort');//Регистрируем корректное завершение
	
	//Проверка на запуск из командной строки
	if(!defined('STDIN'))
		exit;
			
	//Получаем длину пакета данных	
	$len = $argv[1];
	//Считываем пакет данных размером $len
	$input = fread(STDIN, $len);
	//Подготавливаем выходной массив
	$dataList = array();
	//Начинаем разбор данных
	foreach (explode(';',trim($input,';')) as $inputLine)
	{
		$row = explode('|',trim($inputLine,'|'));
		$data = new ParamData();
		$data->ParamId = (int)$row[0];
		$data->DataSrcId = (int)$row[1];
		$data->RegisterDate = $row[2];
		$data->Value = (float)$row[3];
		
		array_push($dataList,$data);
	}
	
	//Сохранение данных в базу
	DataManager::SaveDataArray($dataList);
?>
