#!/usr/bin/php
<?php

	/**
	 * @deprecated (Переход на работу через dbgateway)
	 */
	
	//Выборка данных
	//для обработки в адаптерах
	//D.Obrazcov	

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	register_shutdown_function('DataHelper::abort');//Регистрируем корректное завершение
	
	//Проверка на запуск из командной строки
	if(!defined('STDOUT'))
		exit;
	
	//Считываем входные аргументы
 	$channelId = $argv[1]; //Идентификатор канала
 	$paramId = $argv[2]; //Идентификатор параметра
 	$sort = $argv[3]; // Тип сортировки
 	$rowCount = $argv[4]; //Кол-во возвращаемых строк
 	$condition = $argv[5]; //Условие сравнения дат
 	$comparisonKind = $argv[6]; //Тип сравнения дат
 	$targetDate1 = $argv[7]; //Целевая дата 1
 	$targetDate2 = $argv[8]; //Целевая дата 2

	//Итоговый SQL
	$SQL = " select 
		   		ChannelId 
		   		,ParamId 
		   		,RegisterDate 
		   		,Value 
		     from tblParamData ";
	$orderSQL = " order by RegisterDate ";
	$whereSQL = '';
	
	//Формирование запроса
	//Канал
	if($channelId != 'X')
		$whereSQL = " ChannelId = ".$channelId;
	//Параметр
	if($paramId != 'X')
	{
		if ($whereSQL != '')
			$whereSQL .= ' AND ';
		$whereSQL .= " ParamId = ".$paramId;
	}
	//Дата регистрации
	if($condition != 'X')
	{
		if($whereSQL != '')
			$whereSQL .= ' AND ';
		if ($condition == 'BETW')
		{
			$whereSQL .= ' '.$comparisonKind.'(RegisterDate) BETWEEN '.$comparisonKind."('".$targetDate1."') AND ".$comparisonKind."('".$targetDate2."') ";	
		}
		else 
		{
			$whereSQL .= ' '.$comparisonKind."('".$targetDate1."') ".$condition.' '.$comparisonKind.'(RegisterDate) ';
		}
	}
	if($whereSQL != '')
		$SQL .= ' where '.$whereSQL;
	//Сортировка
	$SQL .= $orderSQL.$sort;
	//Ограничение записей
	if($rowCount != 'X')
		$SQL .= ' limit '.$rowCount;
	
	//Выполнение запроса
	$dbResult = DataHelper::executeQueryOnDataDB($SQL);
	
	//Обработка результата
	$result = '';
	foreach ($dbResult as $row)
	{
		$result .= "\n".$row['ChannelId'].'|'.$row['ParamId'].'|'.$row['RegisterDate'].'|'.$row['Value'];
	}
	
	//Пишем в поток
	fwrite(STDOUT,trim($result));
?>
