#!/usr/bin/php
<?php
	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	register_shutdown_function('DataHelper::abort');//Регистрируем корректное завершение
	
	//Проверка на запуск из командной строки
	if(!defined('STDOUT'))
		exit;
	
	//Получаем информацию из базы
	$controller = ControllerManger::getController(false);
	
	//Пишем в поток
	fwrite(STDOUT,$controller->NETSettings());
?>
