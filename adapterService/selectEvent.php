#!/usr/bin/php
<?php
	
	/**
	 * @deprecated (Переход на работу через dbgateway)
	 */
	//Выборка событий
	//для обработки в адаптерах
	//D.Obrazcov	

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	register_shutdown_function('DataHelper::abort');//Регистрируем корректное завершение
	
	//Проверка на запуск из командной строки
	if(!defined('STDOUT'))
			exit;
	
	//Считываем входные аргументы
 	$channelId = $argv[1]; //Идентификатор канала
 	$typeId = $argv[2]; //Идентификатор типа события
 	$sort = $argv[3]; // Тип сортировки
 	$rowCount = $argv[4]; //Кол-во возвращаемых строк
 	$condition = $argv[5]; //Условие сравнения дат
 	$comparisonKind = $argv[6]; //Тип сравнения дат
 	$targetDate1 = $argv[7]; //Целевая дата 1
 	$targetDate2 = $argv[8]; //Целевая дата 2
 	$argument1 = $argv[9] ? $argv[9] : 'X';
 	$argument2 = $argv[10] ? $argv[10] : 'X';
 	$argument3 = $argv[11] ? $argv[11] : 'X';
 	$argument4 = $argv[12] ? $argv[12] : 'X';
	
	//Итоговый SQL
	$SQL = " select 
		   		ChannelId 
		   		,TypeId 
		   		,RegisterDate 
		   		,case when Argument1 is null then '' else Argument1 end as Argument1
				,case when Argument2 is null then '' else Argument2 end as Argument2
				,case when Argument3 is null then '' else Argument3 end as Argument3
				,case when Argument4 is null then '' else Argument4 end as Argument4 
				,StartEventId
		     from tblEventBus ";
	$orderSQL = " order by RegisterDate ";
	$whereSQL = '';
	
	//Формирование запроса
	//Канал
	if($channelId != 'X')
		$whereSQL = " ChannelId = ".$channelId;
	//Параметр
	if($paramId != 'X')
	{
		if ($whereSQL != '')
			$whereSQL .= ' AND ';
		$whereSQL .= " TypeId = ".$typeId;
	}
	//Дата регистрации
	if($condition != 'X')
	{
		if($whereSQL != '')
			$whereSQL .= ' AND ';
		if ($condition == 'BETW')
		{
			$whereSQL .= ' '.$comparisonKind.'(RegisterDate) BETWEEN '.$comparisonKind."('".$targetDate1."') AND ".$comparisonKind."('".$targetDate2."') ";	
		}
		else 
		{
			$whereSQL .= ' '.$comparisonKind."('".$targetDate1."') ".$condition.' '.$comparisonKind.'(RegisterDate) ';
		}
	}
	//Аргумент 1
	if($argument1 != 'X')
	{
		if ($whereSQL != '')
			$whereSQL .= ' AND ';
		$whereSQL .= " Argument1 = " . "\"$argument1\"";
	}
	//Аргумент 2
	if($argument2 != 'X')
	{
		if ($whereSQL != '')
			$whereSQL .= ' AND ';
		$whereSQL .= " Argument2 = " . "\"$argument2\"";
	}
	//Аргумент 3
	if($argument3 != 'X')
	{
		if ($whereSQL != '')
			$whereSQL .= ' AND ';
		$whereSQL .= " Argument3 = " . "\"$argument3\"";
	}
	//Аргумент 4
	if($argument4 != 'X')
	{
		if ($whereSQL != '')
			$whereSQL .= ' AND ';
		$whereSQL .= " Argument4 = " . "\"$argument4\"";
	}
	if($whereSQL != '')
		$SQL .= ' where '.$whereSQL;	
	//Сортировка
	$SQL .= $orderSQL.$sort;
	//Ограничение записей
	if($rowCount != 'X')
		$SQL .= ' limit '.$rowCount;
	
	//Выполнение запроса
	$dbResult = DataHelper::executeQueryOnEventDB($SQL);
	
	//Обработка результата
	$result = '';
	$argument1 = '';
	$argument2 = '';
	$argument3 = '';
	$argument4 = '';
	foreach ($dbResult as $row)
	{
		$argument1 = $row['Argument1'];
		if($argument1 == '')
			$argument1 = 'X';
		$argument2 = $row['Argument2'];
		if($argument2 == '')
			$argument2 = 'X';
		$argument3 = $row['Argument3'];
		if($argument3 == '')
			$argument3 = 'X';
		$argument4 = $row['Argument4'];
		if($argument4 == '')
			$argument4 = 'X';
			
		$result .= "\n".$row['ChannelId'].'|'.$row['TypeId'].'|'.$row['RegisterDate'].'|'.$argument1.'|'.$argument2.'|'.$argument3.'|'.$argument4.
				'|'.$row['StartEventId'];
	}
	
	//Пишем в поток
	fwrite(STDOUT,trim($result));
?>
