#!/usr/bin/php
<?php
	//Получение настроек каналов
	//D.Obrazcov
	
	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();

	register_shutdown_function('DataHelper::abort');//Регистрируем корректное завершение
	
	//Проверка на запуск из командной строки
	if(!defined('STDOUT'))
		exit;

	//Результат
	$resultStr = '';
	//Получаем идентификатор устройства	
	$deviceId = $argv[1];
		
	//Получение настроек канала	
	$channelList = ChannelManager::getChannelByDeviceId($deviceId);
	//Обработка каналов
	foreach ($channelList as $channel)
	{
		$resultStr .= "\n".$channel->getSettingsForController();
	}
	//Пишем в поток
	fwrite(STDOUT,trim($resultStr));
?>
