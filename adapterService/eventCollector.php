#!/usr/bin/php
<?php

	/**
	 * @deprecated (Переход на работу через dbgateway)
	 */

	//Прием событий от адаптеров
	//D.Obrazcov
	
	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();

	register_shutdown_function('DataHelper::abort');//Регистрируем корректное завершение
	
	//Проверка на запуск из командной строки
	if(!defined('STDIN'))
		exit;
		
	//Получаем длину пакета данных	
	$len = $argv[1];
	//Считываем пакет данных размером $len
	$input = fread(STDIN, $len);
	//Подготавливаем выходной массив
	$eventList = array();
	//Начинаем разбор данных
	foreach (explode(';',trim($input,';')) as $inputLine)
	{
		$row = explode('|',trim($inputLine,'|'));
		$event = new Event();
		$event->TypeId = (int)$row[0];
		$event->DataSrcId = (int)$row[1];
		$event->RegisterDate = $row[2];
		$event->Argument1 = $row[3];
		$event->Argument2 = $row[4];
		$event->Argument3 = $row[5];
		$event->Argument4 = $row[6];
		
		array_push($eventList,$event);
	}
	
	//Сохранение событий в базу
	EventManager::saveEvents($eventList);
?>
