<?php
	
	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	try
	{
		$adapterExec = $_POST['adapterExec'];
		$addSP = $_POST['addSP'];
		
		if($addSP == true)
			$adapterExec = str_replace('-sp', '-sp "'.realpath(dirname(__FILE__).'/../adapterService/').'"', $adapterExec);
		
		$adapterExec = str_replace('\\', '', $adapterExec);
		$execFile = Config::ADAPTERS_PATH().$adapterExec;
		
		$output = array();
		unset($output);
		
		exec($execFile,$output);
		
		$XML="<Res Msg='%s'/>";
		$msg = "";
		
		if(count($output) > 0)
		{
			$msg = implode("\r\n", $output);
		}
							
		$XML = sprintf($XML,$msg);
		
		print (gzdeflate($XML,9));
	
	} catch (Exception $e) 
	{
		$error = new Error($e->getMessage());
		print (gzdeflate($error->toXML(),9));
	}
?>