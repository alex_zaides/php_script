<?php

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	$tempFile = $_FILES['file']['tmp_name'];
	$fileName = $_FILES['file']['name'];
	
	$result = "<OK/>";
	$counter = 3; //Счетчик попыток перемещения, на случай если база используется
	
	if(is_uploaded_file($tempFile))
	{
		$controller = ControllerManger::getController(false);
		$compressedFile = Config::DB_NAME().'t';
		
		if(move_uploaded_file($tempFile, $compressedFile))
		{
			$data = gzinflate(file_get_contents($compressedFile));
			
			while($counter > 0)
			{
				if(file_put_contents(Config::DB_NAME(), $data, LOCK_EX) > 0)
				{
					$counter = 0;	
					try
					{
						//0.Проверить версию таблицы tblControllerInfo
						ControllerManger::upgradeControllerTable();
						//1.Восстановить сетевые настройки
						ControllerManger::saveNetworkSettings($controller);
						//2.Переинициализация контроллера после изменений
						ControllerManger::reinitController();
						//3.Изменяем крон файл
						CronManager::createCronTab();
						//4.Копируем на флэш
						exec(Config::SCRIPT_PATH().'toflash.sh -n');
					} 
					catch (Exception $e)
					{
						$error = new Error($e->getMessage());
						$result = $error->toXML();
						
						//Откат изменений
						//Проверяем наличие файла бэкапа на флешке
						if (file_exists(Config::DB_NAME_BACKUP()))
						{
							//Восстанавливаем бэкап из флешки
							copy(Config::DB_NAME_BACKUP(), Config::DB_NAME());
						}
					}
				}
				else
				{
					$counter--;
					if($counter <= 0)
					{
						$error = new Error('Не удалось записать распакованный файл.');
						$result = $error->toXML();
						
					}
				}
			}
			//Удаляем сжатый файл
			if(file_exists($compressedFile))
				unlink($compressedFile);
		}
		else
		{
			$error = new Error('Не удалось переместить файл.');
			$result = $error->toXML();
		}
		
	}
	else 
	{
		$error = new Error('Не удалось загрузить файл.');
		$result = $error->toXML();
	}
	print ($result);
?>