<?php

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();

 	try 
	{
		$userInfo=new UserInfo();
		$userInfo->Login=$_POST['login'];
		$userInfo->PWD=$_POST['pwd'];
		$userInfo=UserManger::registerUser($userInfo);
		$result='';
		if($userInfo->UserID>0)
		{
			$output = array();
			exec("ik-info -v", $output);
			if(count($output) > 0)
				$userInfo->Version = $output[0];
				
			$result=$userInfo->toXML();
		}
		else  
		{
			$error=new Error("Пользователь не опознан");
			$result=$error->toXML();
		}

		print(gzdeflate($result,9));
	} 
	catch (Exception $e) 
	{
		$error = new Error($e->getMessage());
		print (gzdeflate($error->toXML(),9));
	}
?>