<?php

	$output = array();
	$script_name = basename(__FILE__,'php').'ph[p]';
	exec("ps | grep -c '$script_name'",$output);
	if(!isset($output[0]) || $output[0] > 1)
	{
		$err = new Error("Скрипт уже выполняется...");
		print (gzdeflate($err->toXML(),9));
		return;
	}
	
	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	try
	{
		$xml=stripslashes(gzinflate(base64_decode($_POST['controllerXML'])));
		/*$xml=stripslashes ("<Controller ControllerId='1' Address='sdfsdfsdf!!!' TimeZone='-7'>
								<Device DeviceId='31' Address='Тестовое устройство 1' AdapterName='IC_Adapter.exe' Frequency='5'>
									<Sensor SensorId='-6' DeviceId='-2' Address='Тестовый сенсор'>
										<Channel ChanelId='14' ParamId='700' OverridenParamId='789' SensorId='-6' DeviceId='-2' Address='Канал 2' InterfaceArgument1='100' InterfaceArgument2='32.2' InterfaceArgument3='X' InterfaceArgument4='X' IsEvent='0' IsOn='1' />
										<Channel ChanelId='-8' ParamId='405' OverridenParamId='-1' SensorId='-6' DeviceId='-2' Address='Канал 3' InterfaceArgument1='X' InterfaceArgument2='X' InterfaceArgument3='X' InterfaceArgument4='2.3' IsEvent='1' IsOn='1'>
											<EventLim Tag='MIN_VAL' Value='0.34' />
										</Channel>
									</Sensor>
									<AdditionalArgument Name='comNumber' DeviceId='-2' Value='com1' />
									<AdditionalArgument Name='startDelay' DeviceId='-2' Value='1' />
									<Channel ChanelId='-5' ParamId='100' OverridenParamId='-1' SensorId='0' DeviceId='-2' Address='Канал 1' InterfaceArgument1='X' InterfaceArgument2='X' InterfaceArgument3='X' InterfaceArgument4='X' IsEvent='0' IsOn='1' />
								</Device>
								<Device DeviceId='-9' Address='Не указан' AdapterName='IC_Adapter.exe' Frequency='0' />
							</Controller>");*/
		
		/*$xml=stripslashes ("<Controller ControllerId='1' Address='sdfsdfsdf' TimeZone='-7'>
								<Device DeviceId='-2' Address='Тестовое устройство 1' AdapterName='IC_Adapter.exe' Frequency='5'>
									<Channel ChanelId='-5' ParamId='100' OverridenParamId='-1' SensorId='0' DeviceId='-2' Address='Канал 1' InterfaceArgument1='X' InterfaceArgument2='X' InterfaceArgument3='X' InterfaceArgument4='X' IsEvent='0' IsOn='1' />
								</Device>
							</Controller>");*/
		if($xml == '')
		{
			$err = new Error("Запрос не содержит данных");
			print (gzdeflate($err->toXML(),9));
			return;
		}
		
		$doc = simplexml_load_string($xml);
		$userController=$doc[0];
		
		$controller = new Controler();
		
		// обновить поля контроллера
		$controller->Address=$userController['Address'];
		$controller->TimeZone=$userController['TimeZone'];
		
		ControllerManger::saveController($controller);
		file_put_contents("/etc/TZ", 'UTC'.($controller->TimeZone*-1)."\n");
		
		//Сохранение устройств
		foreach ($userController->Device as $deviceArr)
		{
			$device = new Device();
			$device->AdapterName = (string) $deviceArr['AdapterName'];
			$device->Address = (string) $deviceArr['Address'];
			$device->DeviceId = (int) $deviceArr['DeviceId'];
			$device->Frequency = (int) $deviceArr['Frequency'];
			$device->IsOn = (int) $deviceArr['IsOn'];
			if($device->DeviceId < 0)
			{
				$device->DeviceId = DeviceManager::insertDevice($device);
			}
			else 
			{
				DeviceManager::saveDevice($device);
			}
			
			//Сохранение аргументов запуска
			foreach ($deviceArr->AdditionalArgument as $argumentArr)
			{
				$argument = new AdditionalArgument();
				$argument->DeviceId = (int) $device->DeviceId;
				$argument->Name = (string) $argumentArr['Name'];
				$argument->Value = (string) $argumentArr['Value'];
				
				array_push($device->Arguments, $argument);
			}
			
			AdditionalArgumentManager::updateArguments($device->Arguments, $device->DeviceId);
			
			//Сохранение сенсоров
			foreach ($deviceArr->Sensor as $sensorArr)
			{
				$sensor = new SensorInfo();
				$sensor->SensorId = (int)$sensorArr['SensorId'];
				$sensor->Address = (string)$sensorArr['Address'];
				$sensor->DeviceId = (int)$device->DeviceId;
				$sensor->SerialKey = (string)$sensorArr['SerialKey'];
				
				if($sensor->SensorId < 0)
				{
					$sensor->SensorId = SensorManager::insertSensor($sensor);
				}
				else 
				{
					SensorManager::saveSensor($sensor);
				}
				
				//Сохранение каналов сенсора
				foreach ($sensorArr->Channel as $channelArg)
				{
					$channel = new ChanelInfo();
					$channel->Address = (string) $channelArg['Address'];
					$channel->ChanelId = (int) $channelArg['ChanelId'];
					$channel->DeviceId = $sensor->DeviceId;
					$channel->SensorId = $sensor->SensorId;
					$channel->InterfaceArgument1 = (string)$channelArg['InterfaceArgument1'];
					$channel->InterfaceArgument2 = (string)$channelArg['InterfaceArgument2'];
					$channel->InterfaceArgument3 = (string)$channelArg['InterfaceArgument3'];
					$channel->InterfaceArgument4 = (string)$channelArg['InterfaceArgument4'];
					$channel->IsEvent = (int)$channelArg['IsEvent'];
					$channel->IsOn = (int)$channelArg['IsOn'];
					$channel->OverridenParamId = (int)$channelArg['OverridenParamId'];
					$channel->ParamId = (int)$channelArg['ParamId'];
					$channel->SerialKey = (string)$channelArg['SerialKey'];
					
					if($channel->ChanelId < 0)
					{
						$channel->ChanelId = ChannelManager::insertChannel($channel);
					}
					else
					{ 
						ChannelManager::saveChannel($channel);
					}
					
					//Сохранение границ возникновения событий
											
					foreach ($channelArg->EventSetting as $eventLimVal)
					{
						$eventLim = new EventLimValue();
						$eventLim->Id = (string)$eventLimVal['Id'];
						$eventLim->Type = (string)$eventLimVal['Type'];
						$eventLim->Start = (string)$eventLimVal['DRB'];
						$eventLim->Finish = (string)$eventLimVal['DRE'];
						$eventLim->Value = (string)$eventLimVal['Value'];
						$eventLim->IsHoliday = $eventLimVal['H'];
						array_push($channel->EventVals,$eventLim);
					}
					
					ChannelManager::updateEventLim($channel->ChanelId,$channel->EventVals);
				}
			}
			//Сохранение каналов устройства
			foreach ($deviceArr->Channel as $channelArg)
			{
				$channel = new ChanelInfo();
				$channel->Address = (string) $channelArg['Address'];
				$channel->ChanelId = (int) $channelArg['ChanelId'];
				$channel->DeviceId = $device->DeviceId;
				$channel->SensorId = -1;
				$channel->InterfaceArgument1 = (string)$channelArg['InterfaceArgument1'];
				$channel->InterfaceArgument2 = (string)$channelArg['InterfaceArgument2'];
				$channel->InterfaceArgument3 = (string)$channelArg['InterfaceArgument3'];
				$channel->InterfaceArgument4 = (string)$channelArg['InterfaceArgument4'];
				$channel->IsEvent = (int)$channelArg['IsEvent'];
				$channel->IsOn = (int)$channelArg['IsOn'];
				$channel->OverridenParamId = (int)$channelArg['OverridenParamId'];
				$channel->ParamId = (int)$channelArg['ParamId'];
				$channel->SerialKey = (string)$channelArg['SerialKey'];
				
				if($channel->ChanelId < 0)
				{
					$channel->ChanelId = ChannelManager::insertChannel($channel);
				}
				else 
					ChannelManager::saveChannel($channel);
					
				$channelRemoveList .= $channel->ChanelId . ',';
									
				//Сохранение границ возникновения событий
				foreach ($channelArg->EventSetting as $eventLimVal)
				{
					$eventLim = new EventLimValue();
					$eventLim->Id = (string)$eventLimVal['Id'];
					$eventLim->Type = (string)$eventLimVal['Type'];
					$eventLim->Start = (string)$eventLimVal['DRB'];
					$eventLim->Finish = (string)$eventLimVal['DRE'];
					$eventLim->Value = (string)$eventLimVal['Value'];
					$eventLim->IsHoliday = $eventLimVal['H'];
					array_push($channel->EventVals,$eventLim);
				}
					
				ChannelManager::updateEventLim($channel->ChanelId,$channel->EventVals);
			}
		}
		
		//Переинициализация контроллера после изменений
		ControllerManger::reinitController();
		//Изменяем крон файл
		CronManager::createCronTab();
		
		DataHelper::executeNonQuery("vacuum");
		
		//Копируем на флэш
		exec(Config::SCRIPT_PATH().'toflash.sh -n');
				
		print (gzdeflate('<OK/>',9));
				
	} catch (Exception $e) 
	{
		$error = new Error($e->getMessage());
		print (gzdeflate($error->toXML(),9));
	}
?>