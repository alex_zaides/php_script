<?php

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();

	try
	{
		$actionType=$_POST['actionType'];
		
		$result='';
		
		//сетевые настройки
		if($actionType==0)
		{
			$controller = ControllerManger::getController(false); 
			$result=$controller->toNetSettingXML();
		}
		//мониторинг состояния
		//мониторинг данных
		else
		{
			$controller = ControllerManger::getController(); 
			$result=$controller->toXML();
		}
			
		print (gzdeflate($result,9));
	
	} catch (Exception $e) 
	{
		$error = new Error($e->getMessage());
		print (gzdeflate($error->toXML(),9));
	}
?>