<?php
	
	//Экспорт лог файла
	
	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	
	$archName = '/tmp/logs.tar';
	
	if(file_exists($archName))
		unlink($archName);
	
	exec(Config::SCRIPT_PATH().'logmover.sh');
	exec('tar -cf '.$archName.' '.Config::LOG_FILE_PATH());
	
	$result = file_get_contents($archName);
	
	unlink($archName);
	
	print(gzdeflate($result,9));
?>