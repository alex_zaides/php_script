<?php
	$output = array();
	$script_name = basename(__FILE__,'php').'ph[p]';
	exec("ps | grep -c '$script_name'",$output);
	if(!isset($output[0]) || $output[0] > 1)
	{
		$err = new Error("Скрипт уже выполняется...");
		print (gzdeflate($err->toXML(),9));
		return;
	}

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	try 
	{
		// получить данные из БД
		$controller = new Controler();
	
		// считать XML
		$xml=stripslashes(gzinflate(base64_decode($_POST['controllerXML'])));
				
		if($xml == '')
		{
			$err = new Error("Запрос не содержит данных");
			print (gzdeflate($err->toXML(),9));
			return;
		}
		
		$doc = simplexml_load_string($xml);
		$userController=$doc[0];

		// обновить поля контроллера
		$controller->IP=$userController['IP'];
		$controller->MAC=$userController['MAC'];
		$controller->NetPriority=$userController['NETPriority'];
		$controller->DNS1=$userController['DNS1'];
		$controller->DNS2=$userController['DNS2'];
		$controller->NetMask=$userController['NetMask'];
		$controller->Gateway=$userController['Gateway'];
		$controller->NTPServer=$userController['NTPServer'];
		$controller->UsePPP=$userController['UsePPP'];
		$controller->UsePPP2=$userController['UsePPP2'];
		$controller->PPPLogin=$userController['PPPLogin'];
		$controller->PPPLogin2=$userController['PPPLogin2'];
		$controller->PPPPass=$userController['PPPPass'];
		$controller->PPPPass2=$userController['PPPPass2'];
		$controller->PPPSerial=$userController['PPPSerial'];
		$controller->PPPBaud=$userController['PPPBaud'];
		$controller->APN=$userController['APN'];
		$controller->APN2=$userController['APN2'];
		$controller->CheckServerName=$userController['CheckServerName'];
		$controller->UseVPN=$userController['UseVPN'];
		$controller->VPNType=$userController['VPNType'];
		$controller->VPNLogin=$userController['VPNLogin'];
		$controller->VPNPWD=$userController['VPNPWD'];
		$controller->VPNKeyFileName=$userController['VPNKeyFileName'];
		$controller->VPNConnectServer=$userController['VPNConnectServer'];
		$controller->VPNConnectServer2=$userController['VPNConnectServer2'];
		$controller->VPNPort=$userController['VPNPort'];
		$controller->VPNPort2=$userController['VPNPort2'];
		$controller->HostService=$userController['HostService'];
		$controller->UsePPPoE=$userController['UsePPPoE'];
		$controller->PPPoEType=$userController['PPPoEType'];
		$controller->PPPoEServer=$userController['PPPoEServer'];
		$controller->PPPoELogin=$userController['PPPoELogin'];
		$controller->PPPoEPass=$userController['PPPoEPass'];
			
		// сохранить контроллер в БД
		$controller=ControllerManger::saveNetworkSettings($controller);

		// Копируем на флэш
		exec(Config::SCRIPT_PATH().'toflash.sh -n');
		exec(Config::SCRIPT_PATH().'controller.sh -i "'.realpath(dirname(__FILE__).'/../adapterService/getNetworkSettings.php').'" > /dev/null 2>&1 &');
		
		print (gzdeflate('<OK/>',9));
				
	} catch (Exception $e) 
	{
		$error = new Error($e->getMessage());
		print (gzdeflate($error->toXML(),9));
	}
?>