<?php

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	try
	{
		$XML="<Res Lst='%s'/>";
		$list = "";

		$dirName = Config::SCRIPT_PATH();
			
		if ($handle = opendir($dirName))
		{
			//Перебор файлов
			while (false !== ($entry = readdir($handle)))
			{
				if ($entry != "." && $entry != "..")
				{
					$list .= $entry.";";
				}
			}
			closedir($handle);
			$list = trim($list,";");
		}
		
		$XML=sprintf($XML,$list);
		print (gzdeflate($XML,9));
	}
	catch(Exception $e)
	{
		$error = new Error($e->getMessage());
		print (gzdeflate($error->toXML(),9));
	}
?>