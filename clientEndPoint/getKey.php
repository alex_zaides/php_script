<?php
	
	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	try
	{
		$adapterExec = $_POST['adapterExec'];
		
		$execFile = Config::ADAPTERS_PATH().$adapterExec;
		
		$output = array();
		unset($output);
		
		exec($execFile,$output);
		
		$XML = "<Keys DeviceKey='%s' SensorKey='%s' />";
		$deviceKey = '';
		$sensorKey = '';
		
		$deviceKey = $output[0];
		
		for ((int) $i = 1; $i < count($output); $i++) 
		{
			$sensorKey .= ";;".$output[$i];
		}
		
		$sensorKey = trim($sensorKey,";;");
							
		$XML = sprintf($XML,$deviceKey,$sensorKey);
		
		print (gzdeflate($XML,9));
	
	} catch (Exception $e) 
	{
		$error = new Error($e->getMessage());
		print (gzdeflate($error->toXML(),9));
	}
?>