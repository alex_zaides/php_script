<?php

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();

	try
	{
		$parentId=$_POST['parentId'];
		$isDevice = $_POST['isDevice'];
			
		$list = ChannelManager::getChannelByParentId($parentId, $isDevice);
		
		$result = "<List>";
		
		foreach ($list as $channel)
		{
			$result .= $channel->toXML();
		}
		
		$result .= "</List>";
			
		print (gzdeflate($result,9));
	
	} catch (Exception $e) 
	{
		$error = new Error($e->getMessage());
		print (gzdeflate($error->toXML(),9));
	}
?>