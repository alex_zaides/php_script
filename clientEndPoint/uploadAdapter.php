<?php

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	$tempFile = $_FILES['file']['tmp_name'];
	$fileName = $_FILES['file']['name'];

	if(is_uploaded_file($tempFile))
	{
		if(move_uploaded_file($tempFile, Config::ADAPTERS_PATH().$fileName))
		{
			$output = array();
			
			$adapter = new Adapter();
			$adapter->FileName = $fileName;
			
			try
			{
				// Выставление прав на запуск	
				exec("chmod +x ".Config::ADAPTERS_PATH().$adapter->FileName);
				// Копирование на флэш
				exec("sync");

				//Обращение к адаптеру
				$command = Config::ADAPTERS_PATH().$adapter->FileName." -p";
				
				unset($output);
			
				//Получение информации по адаптеру и параметрам
				exec($command,$output);
				$adapter->AdapterInfo = $output[0];
			
				for ((int) $i = 1; $i < count($output); $i++)
				{
					$adapter->ParamInfo .= ';;'.$output[$i];
				}
					
				$adapter->ParamInfo = trim($adapter->ParamInfo,';;');
					
				//Получение информации по аргументам запуска
				$command = Config::ADAPTERS_PATH().$adapter->FileName." -a";
				unset($output);
				exec($command,$output);
							
				for ((int) $i = 0; $i < count($output); $i++)
				{
					$adapter->AdditionalArgument .= ';;'.$output[$i];
				}
				
				$adapter->AdditionalArgument = trim($adapter->AdditionalArgument,';;');
						 
			} catch (Exception $e)
			{
				$error = new Error($e->getMessage());
				print ($error->toXML()); 
				exit;
			}
			
			print ($adapter->toXML());
			exit;
		}
	}
	$error = new Error('Не удалось загрузить файл '.$fileName);
	print ($error->toXML());
?>