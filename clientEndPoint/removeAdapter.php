<?php

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();

	try
	{
		$adapterFile=$_POST['fileName'];
		$adapterFile = Config::ADAPTERS_PATH().$adapterFile;
		
		if(file_exists($adapterFile))
		{ 
			unlink($adapterFile);
			exec('sync');
		}
		print (gzdeflate("<OK/>",9));
	
	} catch (Exception $e) 
	{
		$error = new Error($e->getMessage());
		print (gzdeflate($error->toXML(),9));
	}
?>