<?php

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();

	try
	{
		$holiday = stripslashes(gzinflate(base64_decode($_POST['HolidayXml'])));
		$doc = simplexml_load_string($holiday);
		$userHoliday=$doc[0];
		$Year = $userHoliday->Year; 
		$Days = new Holidays($Year['V']);

		foreach ($Year->Day as $dayXML){
			$day = new Holiday();	
			$day->Days = $dayXML['V'];
			array_push($Days->Days,$day);
		}
		HolidayManager::SaveHoliday($Days);
                //Копируем на флэш
		exec(Config::SCRIPT_PATH().'toflash.sh -n');

	} catch (Exception $e) 
	{
		$error = new Error($e->getMessage());
		print (gzdeflate($error->toXML(),9));
	}
	print (gzdeflate('<OK/>',9));
?>