<?php

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	try
	{
		$scriptName=$_POST['scriptName'];
		$output = array();
		
		$command = Config::SCRIPT_PATH().$scriptName;
		
		if(file_exists($command))
		{
			exec($command,$output);
		}
		else 
		{
			throw new Exception('Файл "'.$command.'" не существует');
		}
		
		$XML="<Res Msg='%s'/>";
		$msg = "";
		
		if(count($output) > 0)
		{
			$msg = implode("\r\n", $output);
		}
		
		$XML=sprintf($XML,$msg);
		print (gzdeflate($XML,9));
	}
	catch(Exception $e)
	{
		$error = new Error($e->getMessage());
		print (gzdeflate($error->toXML(),9));
	}
	
?>