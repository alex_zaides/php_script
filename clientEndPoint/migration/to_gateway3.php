<?php	
	$output = array();
	$script_name = basename(__FILE__,'php').'ph[p]';
	exec("ps | grep -c '$script_name'",$output);
	if(!isset($output[0]) || $output[0] > 1)
	{
		$err = new Error("Скрипт уже выполняется...");
		print (gzdeflate($err->toXML(),9));
		return;
	}

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();


	DataHelper::executeNonQuery('
		CREATE TABLE "tblEventLimInterval" 
		(
			"IntervalId"	integer		NOT NULL PRIMARY KEY AUTOINCREMENT,
			"ChannelId"		integer 	not null,
			"LimId"			integer		NOT NULL,
			"StartTime"		text(25)	NOT NULL,
			"EndTime"		text(25)	NOT NULL,
			"Days"			integer		NOT NULL,
			"Params"		text(500)	NULL,
			"IsOn"			integer		NOT NULL DEFAULT \'1\',
			FOREIGN KEY(ChannelId) REFERENCES tblChanelInfo(ChanelId) ON DELETE CASCADE
		);
		'
	);		
	
	DataHelper::executeNonQueryOnEventDB('
		alter table tblEventBus add column   "StartEventId"		integer NULL ;	
		CREATE TABLE "tblEventLimAggr" 
		(
			"Type"			integer not null,
			"ChannelId"		integer not null,
			"OnDate"		text(25) not NULL,
			"AggValue"		numeric 	NULL,
			"AggCnt"		integer		NOT NULL DEFAULT 1,
			"OnEventId"     integer     NULL, 
			"IsOn"	        integer     NULL DEFAULT 0,
			PRIMARY KEY ("Type", "ChannelId")
		);
		'
	);
	exec(Config::SCRIPT_PATH().'toflash.sh -n');
	echo 'OK';
?>	