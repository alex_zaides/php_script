<?php	
	$output = array();
	$script_name = basename(__FILE__,'php').'ph[p]';
	exec("ps | grep -c '$script_name'",$output);
	if(!isset($output[0]) || $output[0] > 1)
	{
		$err = new Error("Скрипт уже выполняется...");
		print (gzdeflate($err->toXML(),9));
		return;
	}

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();


	DataHelper::executeNonQuery('
		CREATE TABLE "tblEventHolyday" 
		(
			"HolydayId"		integer		NOT NULL PRIMARY KEY AUTOINCREMENT,
			"Days"			text(25)	NOT NULL
		);	
		alter table tblEventLimInterval add column   "IsHoliday"	integer	NOT NULL DEFAULT "0" ;		
		'
	);		
	exec(Config::SCRIPT_PATH().'toflash.sh -n');
	echo 'OK';
?>	