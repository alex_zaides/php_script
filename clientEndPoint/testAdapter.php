<?php

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	$output = array();
	
	try
	{
		$adapterFile=$_POST['fileName'];
		$adapterFile = Config::ADAPTERS_PATH().$adapterFile;
				
		if(file_exists($adapterFile) === false)
		{
			$error = new Error('Тестируемый файл адаптера не существует.');
			print (gzdeflate($error->toXML(),9));
			exit;
		}
		
		$command = $adapterFile.' %s';
		LogManager::AddRecord(sprintf("Выполняю команду %s -p", $command));
		$result = "<AdapterTest Param='%s' Argument='%s' Cmd='%s' Key='%s' Test='%s'/>";
		
		//Получение информации по адаптеру и параметрам
		unset($output);
		exec(sprintf($command,'-p'),$output);
		
		for ((int) $i = 0; $i < count($output); $i++)
		{
			$param .= ';;'.$output[$i];
		}
		$param = trim($param,';;');
			
		//Получение информации по аргументам запуска
		unset($output);
		exec(sprintf($command,'-a'),$output);
			
		for ((int) $i = 0; $i < count($output); $i++)
		{
			$argument .= ';;'.$output[$i];
		}
		$argument = trim($argument,';;');
		
		//Получение информации по командам
		unset($output);
		exec(sprintf($command,'-cmd'),$output);
			
		for ((int) $i = 0; $i < count($output); $i++)
		{
			$cmd .= ';;'.$output[$i];
		}
		$cmd = trim($cmd,';;');
		
		//Получение информации по ключам оборудования
		unset($output);
		exec(sprintf($command,'-d'),$output);
			
		for ((int) $i = 0; $i < count($output); $i++)
		{
			$key .= ';;'.$output[$i];
		}
		$key = trim($key,';;');
		
		//Получение информации по тесту оборудования
		unset($output);
		exec(sprintf($command,'-t'),$output);
			
		for ((int) $i = 0; $i < count($output); $i++)
		{
			$test .= ';;'.$output[$i];
		}
		$test = trim($test,';;');
		
		$result = sprintf($result,$param,$argument,$cmd,$key,$test);
		print (gzdeflate($result,9));
	
	} catch (Exception $e)
	{
		$error = new Error($e->getMessage());
		print (gzdeflate($error->toXML(),9));
	}
?>