<?php

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	try
	{
		/*
		 * Строка. Набор идентификаторов удаляемых объектов данного типа. 
		 * Разделитель ';;'
		 */
		$ids_string = $_POST['objectId'];
		
		$ids_array = explode(";;", $ids_string);
		
		/*
		 Тип удаляемого объекта
		1 - устройство
		2 - сенсор
		3 - канал
		*/
		$objectType = $_POST['objectType'];
		
		for ((int) $i = 0; $i < count($ids_array); $i++)
		{
			$id = $ids_array[$i];
			
			if($objectType == 1)
			{
				DeviceManager::removeDevice($id);
			}
			else if($objectType == 2)
			{
				SensorManager::removeSensor($id);
			}
			else if($objectType == 3)
			{
				ChannelManager::removeChannel($id);
			}
		}

		// Если удалили устройства - перестроение кронтаба
		if($objectType == 1)
		{
			CronManager::createCronTab();
		}		
		
		//Переинициализация контроллера после изменений
		ControllerManger::reinitController();
						
		//Копируем на флэш
		exec(Config::SCRIPT_PATH().'toflash.sh -n');
		
		print (gzdeflate("<OK/>",9));
		
	} catch (Exception $e) 
	{
		$error = new Error($e->getMessage());
		print (gzdeflate($error->toXML(),9));
	}
?>