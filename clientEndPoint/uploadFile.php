<?php

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	
	$tempFile = $_FILES['file']['tmp_name'];
	$fileName = $_FILES['file']['name'];
	
	$result = "<OK/>";
	
	if(is_uploaded_file($tempFile) 
		&& move_uploaded_file($tempFile, Config::VPN_KEY_PATH().$fileName))
	{
		exec('sync');
	}
	else
	{
		$error = new Error('Не удалось загрузить файл '.$fileName);
		$result = $error->toXML();
	} 
	
	print ($result);
?>