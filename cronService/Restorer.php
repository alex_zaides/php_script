<?php

	//Восстановитель данных после перезапуска
	//Author: D.Obrazcov

	// Проверка на возможность запуска и запуск
	$output = array();
	$script_name = basename(__FILE__,'php').'ph[p]';
	exec("ps | grep -c '$script_name'",$output);
	if(!isset($output[0]) || $output[0] > 1)
		return;

	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	require_once 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	//Проверяем наличие файла бэкапа на флешке
	if (file_exists(Config::DB_BACKUP_NAME())) 
	{
		//Проверяем размер файла бэкапа
		$size = filesize(Config::DB_BACKUP_NAME());
		$broken = ($size == 0);
			
		if($broken)
		{
			//Восстанавливаем чистую БД
			copy(Config::DB_DATA_NAME(), Config::DB_BACKUP_NAME());
		}
		else
		{
			//Восстанавливаем бэкап из флешки
			copy(Config::DB_BACKUP_NAME(), Config::DB_DATA_NAME());
		}
	}
	else 
	{
		//Проверяем наличие директории
		if(!file_exists(dirname(Config::DB_BACKUP_NAME())))
    		mkdir(dirname(Config::DB_BACKUP_NAME()), 0777, true);
	}

	//Проверяем наличие папки с архивами
	if(!file_exists(Config::DB_ARCH_PATH()))
    	mkdir(Config::DB_ARCH_PATH(), 0777, true);
	
	//Получение числа событий
	if(file_exists(Config::DB_EVENT_NAME()))
	{
		//Проверяем размер файла бэкапа
		$size = filesize(Config::DB_EVENT_NAME());
					
		if($size == 0)
		{
			//Восстанавливаем чистую БД
			copy(Config::DB_EVENT_NAME_REST(), Config::DB_EVENT_NAME());
		}
	}
	else
	{
		//Создаем директорию если нет
		if(!file_exists(dirname(Config::DB_EVENT_NAME())))
    		mkdir(dirname(Config::DB_EVENT_NAME()), 0777, true);
    			
		//Восстанавливаем из ОЗУ
		copy(Config::DB_EVENT_NAME_REST(), Config::DB_EVENT_NAME());
	}
?>