<?php

	// Обработчик команд
	// Author D.Obrazcov

	// Проверка на возможность запуска и запуск
	$output = array();
	exec('ps | grep -v grep | grep "commandExecutor.php"',$output);
	if(count($output) > 2)
		return;
	
	//Запоминаем текущую директорию
	$now_at_dir = getcwd();
	//Меняем директорию
	chdir(realpath(dirname(__FILE__).'/../lib/'));
	//Подключаем конфиг
	include 'config.php';
	//Возвращаемся назад
	chdir($now_at_dir);
	//Подключаем данные из конфига
	Config::IncludeFiles();
	
	set_time_limit(600); //Устанавливаем таймаут
	register_shutdown_function('DataHelper::abort');//Регистрируем корректное завершение
			
	try 
	{
		$commandList = CommandManager::getNotProceededCommand(); // Список необработанных команд
		$deviceResult = DeviceManager::getDevices(false); // Список устройств
		$deviceList = array(); // Ассоциативный список устройств
		
		$errorMessage = "Command Id=%d Source=%s не обработана. Reason: %s"; // Сообщение об ошибке
		$servicePath = ' -sp "'.realpath(dirname(__FILE__).'/../adapterService/').'"';
		$shellScript = Config::ADAPTERS_PATH().'%s -cmd %d %d "%s" "%s" %d '.$servicePath; // Строка вызова адаптера 
		$execStr = '';
		$argument = " -a %s '%s'"; // Аргументы запуска
		$output = array(); // Массив для ответа адаптера
		
		foreach ($deviceResult as $device)
		{
			if(!array_key_exists($device->DeviceId, $deviceList))
				$deviceList[$device->DeviceId] = $device;
		}
		
		while (count($commandList)>0) 
		{
			foreach ($commandList as $command)
			{
				// Получение устройства
				$device = $deviceList[$command->deviceId];
				if($device == null)
				{
					LogManager::AddRecord(sprintf($errorMessage,$command->commandId,$command->source,"в БД не найдено устройство."), "Error");
					continue;
				}
				
				//Перевод команд в состояние обрабатывается
				$command->state = 6;
				CommandManager::updateState($command);
				
				// Формирование строки запуска
				$execStr = sprintf($shellScript,$device->AdapterName,$command->deviceId, $command->type,$command->message,$command->source,$command->commandId);
				
				// Сбор аргументов запуска
				foreach ($device->Arguments as $arg)
				{
					$execStr .= sprintf($argument,$arg->Name,$arg->Value);
				}
				
				// Запуск
				try 
				{
					unset($output);
					exec($execStr,$output);
										
					// Разбор результата
					$row = explode('|',$output[0]);
					if(count($row) < 2)
						$row = explode(';', trim($output[0],';'));
					
					if(count($row) < 2)
					{
						$command->state = 7;
						$command->result = "Некорректный ответ от адаптера:".str_replace('|', '!', $output[0]);
					}
					else
					{
						$command->state = (int)$row[0];
						$command->result = $row[1];
						if($command->result === '')
							$command->result = '{Empty}';
					}
					$command->endExecDate = Config::GET_DATE();
					
					if(!($command->state == 7) && !($command->state == 8))
					{
						$command->result .= '. Некорректный код состояния ('.$command->state.')';
						$command->state = 7;
					}
					
					//Сохранение нового состояния
					CommandManager::updateState($command);
				} 
				catch (Exception $e) 
				{
					$command->state = 7; //Провалено
					$command->result = $e->getMessage();
					$command->endExecDate = Config::GET_DATE();
					//Сохранение нового состояния
					try {
						CommandManager::updateState($command);
					} catch (Exception $e) {}
					
					LogManager::AddRecord(sprintf($errorMessage,$command->commandId,$command->source,$e->getMessage()));
				}
			}
			// Получение следующих необработанных команд
			$commandList = CommandManager::getNotProceededCommand();
		}
	} 
	catch (Exception $e) 
	{
		LogManager::AddRecord($e->getMessage());
	}
?>